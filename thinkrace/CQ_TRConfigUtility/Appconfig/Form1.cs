﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.IO.Ports;
using System.Threading;

using System.Windows.Forms.PropertyGridInternal;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Globalization;


using Microsoft.VisualBasic;




namespace Appconfig
{
    public partial class Form1 : Form
    {
        private int received_count = 0;       
        private int receivedhex_count = 0;    

        public int CmdIdx = 0;         
        public byte Mtimer = 0;         
        public byte Stimer = 0;         
        public byte RxVoTime = 0;       

        bool IsReceving = false;        
        bool DoingStr = false;          
        bool hexsend1 = true;
        bool ravcesrc1 = false;

        private string version = String.Empty;
        private byte[] updateFileByte = null;
        FileStream fs = null;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // set CarIQ Configuration Name on Button29
            String carIQConfigName = System.Configuration.ConfigurationSettings.AppSettings["CARIQ_CONFIG_NAME"];
            button29.Text = "Configure: !!! " + carIQConfigName + " !!!";
            button30.Text = "Verify: !!! " + carIQConfigName + " !!!";

            // set the background based on the color specified
            String bgcolor = System.Configuration.ConfigurationSettings.AppSettings["BG_COLOR"];
            if (bgcolor != null)
            {
                tabPage1.BackColor = Color.FromName(bgcolor);
                tabPage2.BackColor = Color.FromName(bgcolor);
            }


            try
            {
                foreach (string com in SerialPort.GetPortNames())
                {
                    com1Port.Items.Add(com);
                }
                com1Port.SelectedIndex = 0;

             }
            catch (Exception Err)
            {
                MessageBox.Show("Error " + Err.Message);
            }
        }

        
        private void ReOpenPort()
        {
            try                              
            {
                
                ClosePort();                 
                if (!com1.IsOpen)              
                    btOpen_Click(null, null);
            }
            catch (Exception Err)
            {
                MessageBox.Show(Err.Message, "Error");
            }
        }

        
        /// <summary>
        /// Configure COM Port before sending and receiving data
        /// </summary>
        private void OpenPort()
        {
            CreateLogFile(com1Port.Text+ "Opening ..... ");

            // Data must be written within a second
            com1.WriteTimeout = 1000;                   
            com1.ReadTimeout = 1000;               
            // standard Endline windows style    
            com1.NewLine = "\r\n";                      
            // register event handler to process the data received.
            com1.DataReceived += new SerialDataReceivedEventHandler(this.com1_DataReceived);   
            
            // Port number from the dropdown
            com1.PortName = com1Port.Text;                
            
            // Open the Port for action
            com1.Open();
        }

        
        private void ClosePort()                              
        {

            CreateLogFile(com1Port.Text + "Closing.... ");
            // Remove the Delegate to stop processing
            com1.DataReceived -= this.com1_DataReceived;    
            while (IsReceving)
                Application.DoEvents();                     
            com1.Close();                                    
        }

        /// <summary>
        /// Button to toggle Open and Close of COM Port
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btOpen_Click(object sender, EventArgs e)
        {
            try
            {
                if (com1.IsOpen)
                {
                    
                    ClosePort();                      
                    if (!com1.IsOpen)
                    {
                        btOpen.Text = "Open port";
                    }
                }
                else
                {
                    OpenPort();                   
                    if (com1.IsOpen)
                    {
                        btOpen.Text = "Close Port";
                    }
                }
            }
            catch (Exception er)
            {
                
                ClosePort();                  
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        
        /// <summary>
        /// Delegate method to handle the data received from the COM port
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void com1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                IsReceving = true;
                System.Threading.Thread.Sleep(30);

                int Len = com1.BytesToRead;
                if (Len < 1)
                {
                    IsReceving = false;
                    return;
                }

                byte[] data = new byte[Len];
                com1.Read(data, 0, Len);

                //CreateLogFile("com1_DataReceived Received Data: " + Encoding.GetEncoding("GB2312").GetString(data));
                CreateLogFile("com1_DataReceived Received Data: " + ByteArrayToString(data));


                rtbRecStr.BeginInvoke(new EventHandler(delegate
                {
                    DoingStr = true;


                    string Str = Encoding.GetEncoding("GB2312").GetString(data);


                    received_count += Str.Length;




                    if (ravcesrc1)
                    {
                        if (data.Length > 1)
                        {
                            if ((data[data.Length - 2] == 0x0D) && (data[data.Length - 1] == 0x0A))
                                rtbRecStr.AppendText(Str);
                            else
                                rtbRecStr.AppendText(Str + " \r\n");
                        }
                        else
                        {
                            rtbRecStr.AppendText(Str + " \r\n");
                        }
                    }
                    else
                    {
                        string Hex = string.Empty;
                        receivedhex_count += data.Length;

                        for (int i = 0; i < data.Length; i++)
                        {
                            string tempstr = Convert.ToString(data[i], 16);
                            if (tempstr.Length < 2)
                                tempstr = '0' + tempstr;
                            tempstr = tempstr.ToUpper();
                            Hex += tempstr + ' ';
                        }
                        Hex = Hex.Replace("0D 0A ", "\r\n");


                        CreateLogFile("Received Converted HEX Data: " + Hex);

                        DatasReceived(Hex, null);
                    }


                    if (rtbRecStr.Text.Length > 100000)
                    {
                        rtbRecStr.Text = string.Empty;
                    }

                    DoingStr = false;
                }
                    ));

                while (DoingStr)
                    Application.DoEvents();
                IsReceving = false;
            }
            catch (Exception Err)
            {
                MessageBox.Show(Err.Message, "Error");
            }
        }

        private void btOpen_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (com1.IsOpen)
                {
                    
                    ClosePort();                      
                    if (!com1.IsOpen)
                    {
                        btOpen.Text = "Open Port";
                    }
                }
                else
                {
                    OpenPort();                   
                    if (com1.IsOpen)
                    {
                        btOpen.Text = "Close Port";
                    }
                }
            }
            catch (Exception er)
            {
                
                ClosePort();                  
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        
        private void com1Port_SelectedIndexChanged(object sender, EventArgs e)       
        {
            ReOpenPort();
        }

        
        private void btSend_Click(object sender, EventArgs e)
        {
            
            
            Timeautomatism();
        }
        
        private void Timeautomatism()
        {
            try
            {
                if (!com1.IsOpen)                  
                    btOpen_Click(null, null);
                
                if (hexsend1)
                {
                    string sendhex = tbSendStr.Text;
                    sendhex = DelSpace(sendhex);     
                    SendAsHex(sendhex);

                   // CreateLogFile("Timeautomatism send Data: " + sendhex);
                }
                else
                {
                    Byte[] EncodeByte = Encoding.GetEncoding("GB2312").GetBytes(tbSendStr.Text);   
                    com1.Write(EncodeByte, 0, EncodeByte.Length);
                    com1.Write("\r\n");

                  //  CreateLogFile("Timeautomatism Write Data: " + EncodeByte.ToList().ToString());
                }
            }
            catch (Exception er)
            {
                
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        
        private void SendAsHex(string str)
        {
            byte[] send = new byte[str.Length / 2];     
            int j = 0;
            for (int i = 0; i < str.Length; i = i + 2, j++)
                send[j] = Convert.ToByte(str.Substring(i, 2), 16);
            
            com1.Write(send, 0, send.Length);
            com1.Write("\r\n");

            //CreateLogFile("SendAsHex send Data: " + Encoding.GetEncoding("GB2312").GetString(send));
            CreateLogFile("SendAsHex send Data Array: " + ByteArrayToString(send));
        }

        
        private string DelSpace(string str)
        {
            string TempStr = string.Empty;
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] != ' ') && (str[i] != ','))     
                    TempStr += str[i];
            }

            if (TempStr.Length % 2 != 0)                   
            {
                
                
                    string a = TempStr;
                    TempStr = string.Empty;
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (i == a.Length - 1)
                            TempStr += '0';           
                        TempStr += a[i];
                    }
               
               
               
            }

            //CreateLogFile("DelSpace : " + TempStr);

            return TempStr;
        }

        private string DelSpaceAnd(string str)
        {
            string TempStr = string.Empty;
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] != ' ') && (str[i] != ','))     
                    TempStr += str[i];
            }
            //CreateLogFile("DelSpaceAnd : " + TempStr);

            return TempStr;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {

                CreateLogFile(" Command : Refresh Start");
                string sendhex = "2800000000000000030001000229";

                

                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

               

                System.Threading.Thread.Sleep(200);    
                string sendhex1 = "2800000000000000020001000329";

                

                sendhex = DelSpace(sendhex1);     
                SendAsHex(sendhex1);

                //CreateLogFile("Refresh : " + sendhex1);

                System.Threading.Thread.Sleep(500);    
                string sendhex2 = "2800000000000010060001001729";

                

                sendhex = DelSpace(sendhex2);     
                SendAsHex(sendhex2);

                //CreateLogFile("Refresh : " + sendhex2);

                CreateLogFile(" Command : Refresh Complete");


            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void IDSendbutton_Click(object sender, EventArgs e)
        {
            DatasSendived(IDSend.Text, null);
        }

        
        private void DatasSendived(string data, EventArgs e)
        {
            try
            {
                //CreateLogFile("DatasSendived Data : " + data);
                int i = 0;
                byte[] send = new byte[100];                         
                byte[] sendhex = new byte[100 * 2];                  

                data = DelSpace(data);                               
                
                
                
                if (data.Length != 12)
                {
                  MessageBox.Show("Error ");
                  return;
                }
                send[i++] = 0x28;

                byte[] scrhex = new byte[data.Length];               
                for (int t = 0; t < scrhex.Length; t = t + 2, i++)
                    send[i] = Convert.ToByte(data.Substring(t, 2), 16);

                send[i++] = 0x00;
                send[i++] = 0x03;
                send[i++] = 0x00;
                send[i++] = 0x07;
                send[i++] = 0x01;

                for (int t = 0; t < scrhex.Length; t = t + 2, i++)
                    send[i] = Convert.ToByte(data.Substring(t, 2), 16);

                byte chk = 0;
                for (int t = 1; t < send.Length; t++)
                {
                    chk ^= send[t];
                }

                send[i++] = chk;

                int j = 0;
                sendhex[j++] = 0x28;

                for (int t = 1; t < i; t++)
                {
                    byte c = send[t];
                    byte cx = 0x3D;

                    if (c == 0x28)
                    {
                       sendhex[j++] = 0x3D;
                       sendhex[j++] = (byte)(cx ^ c);
                       
                    }
                    else if (c == 0x29)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else if (c == 0x3D)
                    {
                        sendhex[j++] = c;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else
                    {
                        sendhex[j++] = c;
                    }
                }

                sendhex[j++] = 0x29;

                com1.Write(sendhex, 0, j);

                //CreateLogFile("DatasSendived Hex : " + Encoding.GetEncoding("GB2312").GetString(sendhex));

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        
        private void DatasReceived(string data, EventArgs e)
        {
            try
            {

               // CreateLogFile("DatasReceived data : " + data);


                int i = 0;
                

                data = DelSpace(data);                               
                byte[] scrhex = new byte[data.Length];               
                for (int t = 0; t < scrhex.Length; t = t + 2, i++)
                    scrhex[i] = Convert.ToByte(data.Substring(t, 2), 16);

                CreateLogFile("Hex convert to byte:" + ByteArrayToString(scrhex));

                #region "check valid packect"
                if ((scrhex[0] == 0x28) && (scrhex[i - 1] == 0x29))
                {

                    #region "remove escpae sequence"
                    int j = 1;
                    int t = 1;
                    for (; j < i - 1; j++,t++)
                    {
                        byte c = scrhex[j];
                        //CreateLogFile("Remove Escpae Seq: c:" + c);
                        byte x;
                        if (c == 0x3D)
                        {
                            x = scrhex[j + 1];
                            //CreateLogFile("Remove Escpae Seq: x:" + x);
                            x ^= c;
                            //CreateLogFile("Remove Escpae Seq: x^=c:" + x);
                            scrhex[t] = x;
                            j++;
                        }
                        else
                        {
                            scrhex[t] = c;
                        }
                    }
                    scrhex[t ++] = 0x29;

                    #endregion

                    #region "calculate checksum"
                    int sum = 0;
                    for (i = 1; i < (t - 2); i++)
                    {
                        //CreateLogFile("calculate checksum: scrhex[" + i.ToString() + "]:" + scrhex[i]);

                        sum ^= scrhex[i];

                        //CreateLogFile("calculate checksum: sum:" + sum);

                    }

                    #endregion

                    CreateLogFile("Packet checksum: " + scrhex[t - 2]);

                    #region "Verify checksum"
                    if (sum == scrhex[t - 2])
                    {
                        string Hex = string.Empty;                           
                        byte[] a = new byte[40];
                        byte[] b = new byte[10];
                        byte[] c = new byte[50];

                        CreateLogFile("Primary Type :" + scrhex[7]);
                        CreateLogFile("Secondary Type:" + scrhex[8]);

                        #region "IF Primary Type =0"
                        if (scrhex[7] == 0x00)
                        {
                           
                            switch (scrhex[8])
                            {
                                #region "Basic Information Version"
                                case 0x82:

                                    for (int m = 0; m < 16; m++)
                                    {
                                        a[m] = scrhex[11 + m];
                                    }

                                    Hex = Encoding.GetEncoding("GB2312").GetString(a);  
                                    /*
                                    for (int m = 0; m < 16; m++)
                                    {
                                        string tempstr = Convert.ToString(a[m], 16);
                                        if (tempstr.Length < 2)
                                            tempstr = '0' + tempstr;
                                            Hex += tempstr;
                                    }  */

                                    VerID.Text = Hex;

                                    CreateLogFile("Basic Information: Version:" + Hex); ;
                                    break;
                                #endregion

                                #region "Basic Information ID"
                                case 0x83:
                                    for (int m = 0; m < 6; m++)
                                    {
                                        a[m] = scrhex[11 + m];
                                    }

                                    for (int m = 0; m < 6; m++)
                                    {
                                        string tempstr = Convert.ToString(a[m], 16);
                                        if (tempstr.Length < 2)
                                            tempstr = '0' + tempstr;
                                        Hex += tempstr;
                                    }

                                    log("Setting ID to :" + Hex);
                                    IDSend.Text = Hex;
                                    CreateLogFile("Basic Information: ID:" + Hex); ;
                                    break;
                                #endregion

                                #region "Error"

                                case 0x84:
                                    MessageBox.Show("Error ");
                                    CreateLogFile("Error" ); ;
                                    break;

                                #endregion

                                #region "Power Mode Query"
                                case 0x86:
                                    if (scrhex[11] == 0x01)
                                    {
                                        POWERMODE.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        POWERMODE.SelectedIndex = 1;
                                    }

                                    CreateLogFile("POWERMODE:" + scrhex[11].ToString());
                                    break;

                                #endregion

                                #region "Vibration Level Query"
                                case 0x8F:
                                    Zhendong.SelectedIndex = scrhex[11] - 1;

                                    CreateLogFile("Vibration Level:" + scrhex[11].ToString() + "-1");
                                    break;
                                #endregion

                                #region "Sharp Acceleration"
                                case 0x91:
                                    cmbBoxSpeed.SelectedIndex = scrhex[11] - 1;
                                    break;

                                #endregion

                                #region  "Device Test"
                                case 0xFF:
                                    CreateLogFile("Device Test");
                                    Hex += scrhex[11] / 10;
                                    Hex += scrhex[11] % 10;
                                    GSMNET.Text = Hex;
                                    string Hex1 = string.Empty;                           
                                    Hex1 += scrhex[12] / 10;
                                    Hex1 += scrhex[12] % 10;
                                    GPSNUM.Text = Hex1;
                                    
                                    if ((scrhex[14] & 0x01) == 0x00)
                                    {
                                        HighCan.Text = "OK";
                                    }
                                    else
                                    {
                                        HighCan.Text = "ERROR";
                                    }

                                    if ((scrhex[14] & 0x02) == 0x00)
                                    {
                                        Kline.Text = "OK";
                                    }
                                    else
                                    {
                                        Kline.Text = "ERROR";
                                    }

                                    if ((scrhex[14] & 0x04) == 0x00)
                                    {
                                        GpsModel.Text = "OK";
                                    }
                                    else
                                    {
                                        GpsModel.Text = "ERROR";
                                    }

                                    if ((scrhex[14] & 0x08) == 0x00)
                                    {
                                        GsmModel.Text = "OK";
                                    }
                                    else
                                    {
                                        GsmModel.Text = "ERROR";
                                    }

                                    if ((scrhex[14] & 0x10) == 0x00)
                                    {
                                        Gsenor.Text = "OK";
                                    }
                                    else
                                    {
                                        Gsenor.Text = "ERROR";
                                    }

                                    if ((scrhex[14] & 0x20) == 0x00)
                                    {
                                        Flash.Text = "OK";
                                    }
                                    else
                                    {
                                        Flash.Text = "ERROR";
                                    }

                                    if ((scrhex[14] & 0x40) == 0x00)
                                    {
                                        Rtc.Text = "OK";
                                    }
                                    else
                                    {
                                        Rtc.Text = "ERROR";
                                    }
                                    break;

                                    #endregion
                            }
                        }

                        #endregion

                        #region "IF Primary Type =10"
                        if (scrhex[7] == 0x10)
                        {
                            int n = 11;
                            switch (scrhex[8])
                            {
                                #region "Main Server : Query1"
                                case 0x81:
                                    CreateLogFile("Main Server: Query1" );
                                    for (int m = 0; scrhex[n] != 0x2C; m++,n++)
                                    {
                                        a[m] = scrhex[n];
                                    }
                                    Hex = Encoding.GetEncoding("GB2312").GetString(a);
                                    log("Setting Server IP to :" + Hex);
                                    Mip.Text = Hex;

                                    
                                    n ++;
                                    for (int m = 0; scrhex[n] != 0x2C; m++,n++)
                                    {
                                        b[m] = scrhex[n];
                                    }
                                    Hex = Encoding.GetEncoding("GB2312").GetString(b);
                                    log("Setting Server Port to :" + Hex);
                                    Mport.Text = Hex;
                                    
                                    n ++;
                                    for (int m = 0; n < (scrhex[10] + 11); m++, n++)
                                    {
                                        c[m] = scrhex[n];
                                    }
                                    Hex = Encoding.GetEncoding("GB2312").GetString(c);
                                    Mapn.Text = Hex;
                                    CreateLogFile("APN:" + Hex);
                                    ServerChoice.SelectedIndex = 0;
                                    break;

                                #endregion

                                #region "Main Server : Query2"
                                case 0x82:
                                    CreateLogFile("Main Server: Query2");
                                    for (int m = 0; scrhex[n] != 0x2C; m++,n++)
                                    {
                                        a[m] = scrhex[n];
                                    }
                                    Hex = Encoding.GetEncoding("GB2312").GetString(a);
                                    Mip.Text = Hex;

                                    CreateLogFile("IP:" + Hex);
                                    n ++;
                                    for (int m = 0; scrhex[n] != 0x2C; m++,n++)
                                    {
                                        b[m] = scrhex[n];
                                    }
                                    Hex = Encoding.GetEncoding("GB2312").GetString(b);
                                    Mport.Text = Hex;
                                    CreateLogFile("Port:" + Hex);

                                    n ++;
                                    for (int m = 0; n < (scrhex[10] + 11); m++, n++)
                                    {
                                        c[m] = scrhex[n];
                                    }
                                    Hex = Encoding.GetEncoding("GB2312").GetString(c);
                                    Mapn.Text = Hex;
                                    CreateLogFile("APN:" + Hex);
                                    ServerChoice.SelectedIndex = 1;
                                    break;

                                #endregion

                                #region "APN Setting"

                                case 0x85:
                                    for (int m = 0; scrhex[n] != 0x2C; m++,n++)
                                    {
                                        a[m] = scrhex[n];
                                    }
                                    Hex = Encoding.GetEncoding("GB2312").GetString(a);
                                    User.Text = Hex;

                                    CreateLogFile("APN setting: Username:" + Hex);

                                    n ++;
                                    for (int m = 0; n < (scrhex[10] + 11); m++, n++)
                                    {
                                        b[m] = scrhex[n];
                                    }
                                    Hex = Encoding.GetEncoding("GB2312").GetString(b);
                                    Password.Text = Hex;

                                    CreateLogFile("APN setting: Password:" + Hex);

                                    break;

                                #endregion

                                #region "Basic Information IMEI"
                                case 0x86:
                                    for (int m = 0; m < 15; m++)
                                    {
                                        a[m] = scrhex[11 + m];
                                    }

                                    Hex = Encoding.GetEncoding("GB2312").GetString(a);

                                    log("Setting IMEI to :" + Hex);
                                    IMEI.Text = Hex;
                                    CreateLogFile("Basic Info : IMEI:" + IMEI);
                                    break;
                                #endregion

                                #region "Wifi Switch Query"
                                case 0x89:
                                    if (scrhex[11] == 0x01)
                                    {
                                        WifiSwitch.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        WifiSwitch.SelectedIndex = 1;
                                    }
                                    break;

                                #endregion

                                #region "Wifi Password Query"
                                case 0x8B:
                                    if (scrhex[11] == 0x01)
                                    {
                                        WifiSwitch.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        WifiSwitch.SelectedIndex = 1;
                                    }

                                    for (int m = 0; m < (scrhex[10] - 1); m++)
                                    {
                                        a[m] = scrhex[12 + m];
                                    }
                                    Hex = Encoding.GetEncoding("GB2312").GetString(a);  

                                    WifiPassword.Text = Hex;  
                                    break;

                                #endregion

                                #region "WiFi Name"

                                case 0x8C:
                                    for (int m = 0; m < scrhex[10]; m++)
                                    {
                                        a[m] = scrhex[11 + m];
                                    }
                                    Hex = Encoding.GetEncoding("GB2312").GetString(a);  

                                    WifiName.Text = Hex;  
                                    break;

                                    #endregion
                            }
                        }

                        #endregion

                        #region "IF Primary Type =30(OBD Data reading and AVG Fuel consumption)"

                        if (scrhex[7] == 0x30)
                        {
                            switch (scrhex[8])
                            {
                                #region "OBD Data Reading"
                                case 0x8A:
                                    CreateLogFile("OBD Data reading:" + scrhex[11].ToString());
                                    if (scrhex[11] == 0x01)
                                    {
                                        ReadObd.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        ReadObd.SelectedIndex = 1;
                                    }
                                    break;

                                #endregion

                                #region "AVG Fuel Consumption"
                                case 0x8B:
                                    Hex = (scrhex[11] / 10).ToString("");
                                    Hex += '.';
                                    Hex += (scrhex[11] % 10).ToString("");
                                    Youhao.Text = Hex;

                                    CreateLogFile("AVG Fuel Consumption:" + Hex);
                                    break;
                               #endregion
                            }
                        }

                        #endregion

                        #region "IF Primary Type =60 (Firmware Upgrade)"
                        if (scrhex[7] == 0x60)
                        {
                            switch (scrhex[8])
                            {
                                case 0x01:
                                case 0x02:
                                case 0x03:
                                    break;
                                case 0x81:
                                    if (scrhex[11] == 0x00)
                                    {
                                        MessageBox.Show("Error ");
                                        CreateLogFile("Error scrhex[11]" + scrhex[11]);
                                    }
                                    else
                                    {
                                        MessageBox.Show("Error ");
                                        CreateLogFile("Error scrhex[11]" + scrhex[11]);
                                        ClosePort();
                                        this.com1.BaudRate = 115200;
                                        OpenPort();
                                        
                                        
                                    }
                                    break;

                                case 0x82:
                                    i = scrhex[11] * 256 + scrhex[12];
                                   
                                    DataSend(i);
                                    int n;
                                    if ((fs.Length % 512) == 0)
                                         n = (int)(fs.Length / 512);
                                    else
                                        n = (int)((fs.Length / 512) + 1);
                                    SetTextMessage(100 * i / n);
                                    break;

                                case 0x83:
                                    MessageBox.Show("Error ");
                                    SetTextMessage(0);
                                    
                                    break;
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }
        private void SetTextMessage(int ipos)
        {
            this.progressBar1.Value = Convert.ToInt32(ipos);

            /*if (this.InvokeRequired)
            {
                SetPos setpos = new SetPos(SetTextMessage);
                this.Invoke(setpos, new object[] { ipos });
            }
            else
            {
                this.label1.Text = ipos.ToString() + "/100";
                this.progressBar1.Value = Convert.ToInt32(ipos);
            }*/
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }


        private void button13_Click(object sender, EventArgs e)
        {
            try
            {
                

                string sendhex = "28000000000000007F0001007E29";

                

                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

                //CreateLogFile("Start Test  : " + sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }



        private void button8_Click(object sender, EventArgs e)
        {
            CreateLogFile("Command : Set Server start.... ");
            if (ServerChoice.SelectedIndex == 0)
                DataSetMainIp(Mip.Text, Mport.Text, Mapn.Text,null);    
            
            if(ServerChoice.SelectedIndex == 1)
                DataSetReIp(Mip.Text,Mport.Text, Mapn.Text, null);

            CreateLogFile("Command : Set Server Complete.");
        }

        private void DataSetMainIp(string Ip, string Port, string Apn, EventArgs e)
        {
            try
            {

                CreateLogFile("Server Set IP:" + Ip + ", Port:" + Port + "APN:" + Apn);
                int i = 0;
                byte[] send = new byte[100];                         
                byte[] sendhex = new byte[100]; 
                byte[] IpAddress = new byte[50];                  
                byte[] Portvalue = new byte[10];                  
                byte[] Apnvalue = new byte[50];                  

                Ip = DelSpaceAnd(Ip);
                Port = DelSpaceAnd(Port);
                Apn = DelSpaceAnd(Apn);

                send[i++] = 0x28;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;

                send[i++] = 0x10;
                send[i++] = 0x01;
                send[i++] = 0x00;
                send[i++] = (byte)(3 + Ip.Length + Port.Length + Apn.Length);
                send[i++] = 0x01;
                IpAddress = System.Text.Encoding.Default.GetBytes(Ip);
                Portvalue = System.Text.Encoding.Default.GetBytes(Port);
                Apnvalue = System.Text.Encoding.Default.GetBytes(Apn);

                //CreateLogFile("IpAddress :" + Encoding.GetEncoding("GB2312").GetString(IpAddress));
                //CreateLogFile("Portvalue :" + Encoding.GetEncoding("GB2312").GetString(Portvalue));
                //CreateLogFile("Apnvalue :" + Encoding.GetEncoding("GB2312").GetString(Apnvalue));

                for (int t = 0; t < IpAddress.Length; t ++)
                    send[i++] = IpAddress[t];

                send[i++] = 0x2C;
                for (int t = 0; t < Portvalue.Length; t++)
                    send[i++] = Portvalue[t];

                send[i++] = 0x2C;
                for (int t = 0; t < Apnvalue.Length; t++)
                    send[i++] = Apnvalue[t];

                byte chk = 0;
                for (int t = 1; t < send.Length; t++)
                {
                    chk ^= send[t];
                }
                send[i++] = chk;

                int j = 0;
                sendhex[j++] = 0x28;

                for (int t = 1; t < i; t++)
                {
                    byte c = send[t];
                    byte cx = 0x3D;

                    if (c == 0x28)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);

                    }
                    else if (c == 0x29)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else if (c == 0x3D)
                    {
                        sendhex[j++] = c;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else
                    {
                        sendhex[j++] = c;
                    }
                }

                sendhex[j++] = 0x29;


                com1.Write(sendhex, 0, j);

                CreateLogFile("DataSetMainIp write data Array:" + ByteArrayToString(sendhex));

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                //CreateLogFile("Display Clear....");

                string Data1;

                HighCan.Text = "ERROR";
                Kline.Text = "ERROR";
                GpsModel.Text = "ERROR";
                GsmModel.Text = "ERROR";
                Gsenor.Text = "ERROR";
                Flash.Text = "ERROR";
                Rtc.Text = "ERROR";
                GSMNET.Text = "0";
                GPSNUM.Text = "0";
                IMEI.Text = "000000000000000";
                VerID.Text = "0000000000000000";
                Data1 = IDSend.Text.Remove(10);
                IDSend.Text = Data1 + "00";
                POWERMODE.SelectedIndex = 1;
                ReadObd.SelectedIndex = 1;
                Mip.Text = "";
                Mport.Text = "";
                Mapn.Text = "";
                WifiSwitch.SelectedIndex = 1;
                WifiName.Text = "";
                WifiPassword.Text = "";
                EncryptionMode.SelectedIndex = 1;
                User.Text = "";
                Password.Text = "";
                Zhendong.SelectedIndex = 1;
                Youhao.Text = "0.0";
            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                
                 string sendhex = "2800000000000000060001000729";
                 sendhex = DelSpace(sendhex);     
                 SendAsHex(sendhex);

                //CreateLogFile("Power Saving Mode Query : "+ sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                

                if (POWERMODE.SelectedIndex == 0)
                {
                    string sendhex = "280000000000000006000201010429";
                    sendhex = DelSpace(sendhex);     
                    SendAsHex(sendhex);
                    CreateLogFile("Power Saving Mode Set Mode: "+ POWERMODE.Text+ " Data:"  + sendhex);
                }

                if (POWERMODE.SelectedIndex == 1)
                {
                    string sendhex = "280000000000000006000201000529";
                    sendhex = DelSpace(sendhex);     
                    SendAsHex(sendhex);
                    CreateLogFile("Power Saving Mode Set Mode: " + POWERMODE.Text + " Data:" + sendhex);
                }





            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void POWERMODE_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Mip_TextChanged(object sender, EventArgs e)
        {

        }


        private void DataSetReIp(string Ip, string Port, string Apn, EventArgs e)
        {
            try
            {

                CreateLogFile("DataSetReIp IP: " + Ip + " Port:" + Port + "APN" + Apn);

                int i = 0;
                byte[] send = new byte[100];                         
                byte[] sendhex = new byte[100];
                byte[] IpAddress = new byte[50];                  
                byte[] Portvalue = new byte[10];                  
                byte[] Apnvalue = new byte[50];                  

                Ip = DelSpaceAnd(Ip);
                Port = DelSpaceAnd(Port);
                Apn = DelSpaceAnd(Apn);

                send[i++] = 0x28;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;

                send[i++] = 0x10;
                send[i++] = 0x02;
                send[i++] = 0x00;
                send[i++] = (byte)(3 + Ip.Length + Port.Length + Apn.Length);
                send[i++] = 0x01;
                IpAddress = System.Text.Encoding.Default.GetBytes(Ip);
                Portvalue = System.Text.Encoding.Default.GetBytes(Port);
                Apnvalue = System.Text.Encoding.Default.GetBytes(Apn);

                //CreateLogFile("IpAddress :" + Encoding.GetEncoding("GB2312").GetString(IpAddress));
                //CreateLogFile("Portvalue :" + Encoding.GetEncoding("GB2312").GetString(Portvalue));
                //CreateLogFile("Apnvalue :" + Encoding.GetEncoding("GB2312").GetString(Apnvalue));

                for (int t = 0; t < IpAddress.Length; t++)
                    send[i++] = IpAddress[t];

                send[i++] = 0x2C;
                for (int t = 0; t < Portvalue.Length; t++)
                    send[i++] = Portvalue[t];

                send[i++] = 0x2C;
                for (int t = 0; t < Apnvalue.Length; t++)
                    send[i++] = Apnvalue[t];

                byte chk = 0;
                for (int t = 1; t < send.Length; t++)
                {
                    chk ^= send[t];
                }
                send[i++] = chk;

                int j = 0;
                sendhex[j++] = 0x28;

                for (int t = 1; t < i; t++)
                {
                    byte c = send[t];
                    byte cx = 0x3D;

                    if (c == 0x28)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);

                    }
                    else if (c == 0x29)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else if (c == 0x3D)
                    {
                        sendhex[j++] = c;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else
                    {
                        sendhex[j++] = c;
                    }
                }

                sendhex[j++] = 0x29;


                com1.Write(sendhex, 0, j);

                CreateLogFile("DataSetReIp write data :" + ByteArrayToString(sendhex));

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }


        private void button9_Click(object sender, EventArgs e)
        {
            try
            {

                CreateLogFile("Command : Server Query Start..for serverchoice:"+ ServerChoice.Text);

                string sendhex = "2800000000000010010001001029"; 

                if (ServerChoice.SelectedIndex == 0)
                    sendhex = "2800000000000010010001001029";

                if (ServerChoice.SelectedIndex == 1)
                    sendhex = "2800000000000010020001001329";

                

                

                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

                

                CreateLogFile("Command : Main Server Query Complete.");
                

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                

                string sendhex = "2800000000000010020001001329";

                

                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }


        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                
                string sendhex = "28000000000000300A000201003329";
                if(ReadObd.SelectedIndex == 0)
                  sendhex = "28000000000000300A000201013829";

                if (ReadObd.SelectedIndex == 1)
                  sendhex = "28000000000000300A000201003929";

                

                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                
                string sendhex = "28000000000000300A0001003B29";

                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void LoadUpdateFile(String fileName)
        {
            
            

            try
            {
                
                FileInfo file = new FileInfo(fileName);
                string extension = file.Extension;
                version = file.Name.Substring(0, file.Name.Length - extension.Length);

                updateFileByte = new byte[file.Length];

                
                fs = new FileStream(fileName, FileMode.Open);
                
                fs.Read(updateFileByte, 0, (int)fs.Length);
                button12.Enabled = true;
                button14.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Error ");
                button14.Enabled = false;
                button12.Enabled = false;
                fs.Close();
            }
            /*finally
            {
                try
                {
                    fs.Close();
                }
                catch { }
            }*/
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFilecon = new OpenFileDialog();
                openFilecon.InitialDirectory = "c:\\ ";
                openFilecon.Filter = "Txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFilecon.FilterIndex = 2;
                openFilecon.Title = "选择打开文件的位置 ";
                openFilecon.RestoreDirectory = false;

                
                if (openFilecon.ShowDialog() == DialogResult.OK)
                {
                    string fileName = openFilecon.FileName;
                    LoadUpdateFile(fileName);

                }
                else return;
            }
            catch (Exception Err)
            {
                fs.Close();
                MessageBox.Show("Error " + Err.Message, "Error ");
            }
        }

        private byte[] GetUpdateBytes(byte[] bytes, int offset, int length)
        {
            byte[] updateBytes = new byte[length];

            Array.Copy(bytes, offset, updateBytes, 0, length);

            return updateBytes;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ClosePort();
            this.com1.BaudRate = 10416;
            OpenPort();
            UpgradeBegin(null);
        }

        private void UpgradeBegin(EventArgs e)
        {
            try
            {
                int i = 0;
                int m = 0;
                byte[] send = new byte[100];                         
                byte[] sendhex = new byte[100 * 2];                  

                send[i++] = 0x28;

                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;

                send[i++] = 0x60;
                send[i++] = 0x01;
                send[i++] = 0x00;
                send[i++] = 0x13;
                send[i++] = 0x00;

                for (int t = 0; t < version.Length; t++)
                    send[i++] = (byte)(version[t]);
                
                
                if ((fs.Length % 512) == 0)
                    m = (int)(fs.Length / 512);
                else
                    m = (int)((fs.Length / 512) + 1);

                send[i++] = (byte)(m / 256);
                send[i++] = (byte)(m % 256);
                byte chk = 0;
                for (int t = 1; t < send.Length; t++)
                {
                    chk ^= send[t];
                }

                send[i++] = chk;

                int j = 0;
                sendhex[j++] = 0x28;

                for (int t = 1; t < i; t++)
                {
                    byte c = send[t];
                    byte cx = 0x3D;

                    if (c == 0x28)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);

                    }
                    else if (c == 0x29)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else if (c == 0x3D)
                    {
                        sendhex[j++] = c;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else
                    {
                        sendhex[j++] = c;
                    }
                }

                sendhex[j++] = 0x29;

                CreateLogFile("UpgradeBegin :" + ByteArrayToString(sendhex));

                com1.Write(sendhex, 0, j);
                

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void DataSend(int Num)
        {
            try
            {
                int i = 0;
                int m = 0;
                byte[] send = new byte[600];                         
                byte[] sendhex = new byte[700];                  
                byte[] gethex = new byte[600];                  

                send[i++] = 0x28;

                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;

                send[i++] = 0x60;
                send[i++] = 0x02;
                send[i++] = 0x02;
                send[i++] = 0x03;
                send[i++] = 0x01;
                send[i++] = (byte)(Num / 256);
                send[i++] = (byte)(Num % 256);

                m = (int)(fs.Length / 512);
                if (Num <= m)
                {
                    gethex = GetUpdateBytes(updateFileByte, (Num - 1) * 512, 512);
                    for (int t = 0; t < 512; t++)
                        send[i++] = gethex[t];
                }
                else
                {
                    m = (int)(fs.Length % 512);
                    gethex = GetUpdateBytes(updateFileByte, (Num - 1) * 512, m);
                    for (int t = 0; t < m; t++)
                        send[i++] = gethex[t];
                    for (int t = 0; t < (512 - m); t++)
                        send[i++] = 0xFF;
                }

                byte chk = 0;
                for (int t = 1; t < i; t++)
                {
                    chk ^= send[t];
                }

                send[i++] = chk;

                int j = 0;
                sendhex[j++] = 0x28;

                for (int t = 1; t < i; t++)
                {
                    byte c = send[t];
                    byte cx = 0x3D;

                    if (c == 0x28)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);

                    }
                    else if (c == 0x29)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else if (c == 0x3D)
                    {
                        sendhex[j++] = c;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else
                    {
                        sendhex[j++] = c;
                    }
                }

                sendhex[j++] = 0x29;

                CreateLogFile("DataSend Num :" + Num);

                CreateLogFile("DataSend Bytes :" + ByteArrayToString(sendhex));

                com1.Write(sendhex, 0, j);
                

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
           UpgradeBegin(null);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
                

                if (WifiSwitch.SelectedIndex == 0)
                {
                    string sendhex = "280000000000001009000201011B29";
                    sendhex = DelSpace(sendhex);     
                    SendAsHex(sendhex);
                }

                if (WifiSwitch.SelectedIndex == 1)
                {
                    string sendhex = "280000000000001009000201001A29";
                    sendhex = DelSpace(sendhex);     
                    SendAsHex(sendhex);
                }

                



            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            try
            {
                
                string sendhex = "2800000000000010090001001829";
                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            try
            {
                int i = 0;
                byte[] send = new byte[100];                         
                byte[] sendhex = new byte[100];
                byte[] wifiName = new byte[50];                  

                send[i++] = 0x28;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;

                send[i++] = 0x10;
                send[i++] = 0x0C;
                send[i++] = 0x00;
                send[i++] = (byte)(1 + WifiName.Text.Length);
                send[i++] = 0x01;
                wifiName = System.Text.Encoding.Default.GetBytes(WifiName.Text);

                for (int t = 0; t < WifiName.Text.Length; t++)
                    send[i++] = wifiName[t];

                byte chk = 0;
                for (int t = 1; t < send.Length; t++)
                {
                    chk ^= send[t];
                }
                send[i++] = chk;

                int j = 0;
                sendhex[j++] = 0x28;

                for (int t = 1; t < i; t++)
                {
                    byte c = send[t];
                    byte cx = 0x3D;

                    if (c == 0x28)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);

                    }
                    else if (c == 0x29)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else if (c == 0x3D)
                    {
                        sendhex[j++] = c;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else
                    {
                        sendhex[j++] = c;
                    }
                }

                sendhex[j++] = 0x29;


                com1.Write(sendhex, 0, j);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                
                string sendhex = "28000000000000100C0001001D29";
                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            try
            {
                int i = 0;
                byte[] send = new byte[100];                         
                byte[] sendhex = new byte[100];
                byte[] wifiPass = new byte[50];                  

                send[i++] = 0x28;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;

                send[i++] = 0x10;
                send[i++] = 0x0B;
                send[i++] = 0x00;
                send[i++] = (byte)(2 + WifiPassword.Text.Length);
                send[i++] = 0x01;

                if (EncryptionMode.SelectedIndex == 0)
                    send[i++] = 0x01;
                else if (EncryptionMode.SelectedIndex == 1)
                    send[i++] = 0x02;
                else
                    send[i++] = 0x00;

                wifiPass = System.Text.Encoding.Default.GetBytes(WifiPassword.Text);

                for (int t = 0; t < WifiName.Text.Length; t++)
                    send[i++] = wifiPass[t];

                byte chk = 0;
                for (int t = 1; t < send.Length; t++)
                {
                    chk ^= send[t];
                }
                send[i++] = chk;

                int j = 0;
                sendhex[j++] = 0x28;

                for (int t = 1; t < i; t++)
                {
                    byte c = send[t];
                    byte cx = 0x3D;

                    if (c == 0x28)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);

                    }
                    else if (c == 0x29)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else if (c == 0x3D)
                    {
                        sendhex[j++] = c;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else
                    {
                        sendhex[j++] = c;
                    }
                }

                sendhex[j++] = 0x29;


                com1.Write(sendhex, 0, j);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                
                string sendhex = "28000000000000100B0001001A29";
                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            try
            {
                string sendhex = "2800000000000000040001010429";
                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            try
            {
                string sendhex = "2800000000000000050001010529";
                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            DataSetUserPassWord(User.Text, Password.Text, null); 
        }

        private void DataSetUserPassWord(string user, string password, EventArgs e)
        {
            try
            {
                int i = 0;
                byte[] send = new byte[100];                         
                byte[] sendhex = new byte[100];
                byte[] IpAddress = new byte[50];                  
                byte[] Portvalue = new byte[10];                  
                

                user = DelSpaceAnd(user);
                password = DelSpaceAnd(password);

                send[i++] = 0x28;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;

                send[i++] = 0x10;
                send[i++] = 0x05;
                send[i++] = 0x00;
                send[i++] = (byte)(2 + user.Length + password.Length);
                send[i++] = 0x01;
                IpAddress = System.Text.Encoding.Default.GetBytes(user);
                Portvalue = System.Text.Encoding.Default.GetBytes(password);

                for (int t = 0; t < IpAddress.Length; t++)
                    send[i++] = IpAddress[t];

                send[i++] = 0x2C;
                for (int t = 0; t < Portvalue.Length; t++)
                    send[i++] = Portvalue[t];

                byte chk = 0;
                for (int t = 1; t < send.Length; t++)
                {
                    chk ^= send[t];
                }
                send[i++] = chk;

                int j = 0;
                sendhex[j++] = 0x28;

                for (int t = 1; t < i; t++)
                {
                    byte c = send[t];
                    byte cx = 0x3D;

                    if (c == 0x28)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);

                    }
                    else if (c == 0x29)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else if (c == 0x3D)
                    {
                        sendhex[j++] = c;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else
                    {
                        sendhex[j++] = c;
                    }
                }

                sendhex[j++] = 0x29;


                com1.Write(sendhex, 0, j);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            try
            {
                
                string sendhex = "2800000000000010050001001429";


                

                

                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button25_Click(object sender, EventArgs e)
        {
            try
            {
                
                string sendhex = "28000000000000000F0001000E29";
                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button26_Click(object sender, EventArgs e)
        {
            try
            {
                int i = 0, j;
                byte[] send = new byte[100];                         
                byte[] sendhex = new byte[100];
                byte[] IpAddress = new byte[50];                  
                byte[] Portvalue = new byte[10];                  

                send[i++] = 0x28;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;

                send[i++] = 0x00;
                send[i++] = 0x0F;
                send[i++] = 0x00;
                send[i++] = 0x02;
                send[i++] = 0x01;
                j = Zhendong.SelectedIndex;
                send[i++] = (byte)(j +1);

                byte chk = 0;
                for (int t = 1; t < send.Length; t++)
                {
                    chk ^= send[t];
                }
                send[i++] = chk;
                send[i++] = 0x29;


                com1.Write(send, 0, i);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        /// <summary>
        /// Set Average Fuel Consumption into Device!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button24_Click(object sender, EventArgs e)
        {
            try
            {
                int i = 0, m;
                byte[] send = new byte[100];                         
                byte[] sendhex = new byte[100];
                byte[] IpAddress = new byte[50];                  


                send[i++] = 0x28;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;

                send[i++] = 0x30;
                send[i++] = 0x0B;
                send[i++] = 0x00;
                send[i++] = 0x02;
                send[i++] = 0x01;
                
                m = (int)(Convert.ToDouble(Youhao.Text) * 10);
                send[i++] = (byte)(m);
                

                byte chk = 0;
                for (int t = 1; t < send.Length; t++)
                {
                    chk ^= send[t];
                }
                send[i++] = chk;

                int j = 0;
                sendhex[j++] = 0x28;

                for (int t = 1; t < i; t++)
                {
                    byte c = send[t];
                    byte cx = 0x3D;

                    if (c == 0x28)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);

                    }
                    else if (c == 0x29)
                    {
                        sendhex[j++] = 0x3D;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else if (c == 0x3D)
                    {
                        sendhex[j++] = c;
                        sendhex[j++] = (byte)(cx ^ c);
                    }
                    else
                    {
                        sendhex[j++] = c;
                    }
                }

                sendhex[j++] = 0x29;


                com1.Write(sendhex, 0, j);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        /// <summary>
        /// Query Average Fuel Consumption from Device
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button23_Click(object sender, EventArgs e)
        {
            try
            {
                
                string sendhex = "28000000000000300B0001003A29";
                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button28_Click(object sender, EventArgs e)
        {
            try
            {
                int i = 0, j;
                byte[] send = new byte[100];                         
                
                
                

                send[i++] = 0x28;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;
                send[i++] = 0x00;

                send[i++] = 0x00;
                send[i++] = 0x11;
                send[i++] = 0x00;
                send[i++] = 0x02;
                send[i++] = 0x01;
                j = cmbBoxSpeed.SelectedIndex;
                send[i++] = (byte)(j + 1);

                byte chk = 0;
                for (int t = 1; t < send.Length; t++)
                {
                    chk ^= send[t];
                }
                send[i++] = chk;
                send[i++] = 0x29;

                com1.Write(send, 0, i);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        private void button27_Click(object sender, EventArgs e)
        {
            try
            {
                
                string sendhex = "2800000000000000110001001029";
                sendhex = DelSpace(sendhex);     
                SendAsHex(sendhex);

            }
            catch (Exception er)
            {
                MessageBox.Show("Error " + er.Message, "Error ");
            }
        }

        ///=============================================================================================
        /// CARIQ Configuration Code
        ///=============================================================================================
        // Constant Strings
        const String DEVICEID_GROUP = "DEVICEID_GROUP";
        const String DEVICE_ID = "DEVICE_ID";
        const String IMEI_NUMBER = "IMEI_NUMBER";
        const String VERSION_ID = "VERSION_ID";

        // Primary Server settings
        const String SERVER_GROUP = "SERVER_GROUP";
        const String SERVER_NAME = "SERVER_NAME";
        const String SERVER_PORT = "SERVER_PORT";
        const String APN = "APN";

        // Standby Server settings
        const String SERVER_NAME2 = "SERVER_NAME2";
        const String SERVER_PORT2 = "SERVER_PORT2";
        const String APN2 = "APN2";

 

        /// <summary>
        /// Configure Device using CarIQ settings (Settings from the CarIQTRConfig xml) - for server, apn
        /// Query and Write data of deviceid, imei, version, server, apn, timestamp into a csv file in the installation directory as configreport.csv
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button29_Click(object sender, EventArgs e)
        {
            // Logger Control
            RichTextBox LOGGER_TEXTBOX = rtbRecStr;

            // Controls and Buttons for Device Group
            Button DEVICEID_GROUP_SET_BUTTON = this.IDSendbutton;
            Button DEVICEID_GROUP_QUERY_BUTTON = this.button3;
            Control DEVICEID_TEXTBOX = IDSend;
            Control IMEI_LABEL = IMEI;
            Control VERSION_LABEL = VerID;

            // Device Id Group - to be queried only
            ConfigParamGroup deviceIdGroup = new ConfigParamGroup(DEVICEID_GROUP, null, DEVICEID_GROUP_SET_BUTTON, DEVICEID_GROUP_QUERY_BUTTON, LOGGER_TEXTBOX);
            deviceIdGroup.add(new ConfigParam(DEVICE_ID, IDSend, null, null, LOGGER_TEXTBOX)).
                add(new ConfigParam(IMEI_NUMBER, IMEI_LABEL, null, null, LOGGER_TEXTBOX)).
                add(new ConfigParam(VERSION_ID, VERSION_LABEL, null, null, LOGGER_TEXTBOX));



            // Controls and Buttons for Server Group
            Button SERVER_GROUP_SET_BUTTON = this.button8;
            Button SERVER_GROUP_QUERY_BUTTON = this.button9;
            Control SERVER_NAME_TEXTBOX = Mip;
            Control SERVER_PORT_TEXTBOX = Mport;
            Control APN_TEXTBOX = Mapn;
            ComboBox SERVER_CHOICE_BOX = ServerChoice;
            int MAIN_SERVER_INDEX = 0;
            int STANDBY_SERVER_INDEX = 1; // second server

            // Server Group - to be set and then queried
            ConfigParamGroup serverGroup = new ConfigParamGroup(SERVER_GROUP, SERVER_CHOICE_BOX, SERVER_GROUP_SET_BUTTON, SERVER_GROUP_QUERY_BUTTON, LOGGER_TEXTBOX);
            serverGroup.add(new ConfigParam(SERVER_NAME, SERVER_NAME_TEXTBOX, null, null, LOGGER_TEXTBOX)).
            add(new ConfigParam(SERVER_PORT, SERVER_PORT_TEXTBOX, null, null, LOGGER_TEXTBOX)).
            add(new ConfigParam(APN, APN_TEXTBOX, null, null, LOGGER_TEXTBOX));

            // Read the Parameters from Config file - primary Server
            {
                String serverName = System.Configuration.ConfigurationSettings.AppSettings[SERVER_NAME];
                String serverPort = System.Configuration.ConfigurationSettings.AppSettings[SERVER_PORT];
                String apn = System.Configuration.ConfigurationSettings.AppSettings[APN];

                serverGroup.setParamValue(SERVER_NAME, serverName).setParamValue(SERVER_PORT, serverPort).setParamValue(APN, apn);
                serverGroup.set(MAIN_SERVER_INDEX); // write to Device
            }

            // Read the Parameters from Config file - Standby Server
            {
                String serverName = System.Configuration.ConfigurationSettings.AppSettings[SERVER_NAME2];
                String serverPort = System.Configuration.ConfigurationSettings.AppSettings[SERVER_PORT2];
                String apn = System.Configuration.ConfigurationSettings.AppSettings[APN2];

                if (serverName != null && serverPort != null && apn != null) {
                serverGroup.setParamValue(SERVER_NAME, serverName).setParamValue(SERVER_PORT, serverPort).setParamValue(APN, apn);
                serverGroup.set(STANDBY_SERVER_INDEX); // write to Device
                    }
            }

            //Thread.Sleep(100);
            // Now query parameters from Device and Server Group table and write to file - but before file, write to console
            // Thread.Sleep(100);
            serverGroup.fetch(MAIN_SERVER_INDEX);
            deviceIdGroup.fetch(-1);
            //           MessageBox.Show("CarIQ Configuration is burnt into Device. Please verify!!");
        }

        private bool check(String parameter, String val)
        {
            // Read the Parameters from Config file
            String expected = System.Configuration.ConfigurationSettings.AppSettings[parameter];
            if (expected != null)
            {
                if (expected.Equals(val))
                {
                    return true;
                }
                else
                {
                    MessageBox.Show("Mismatch!!\nParameter: " + parameter + "\nExpected: " + expected + "\nActual: " + val);
                    return false;
                }
            }

            if (val == null || val.Equals("") || val.Contains("xxx"))
            {
                MessageBox.Show("Mismatch!!\nParameter: " + parameter + " value Unexpected: " + val + "\nPlease Hit Green Refresh Button From Basic Information to fetch Device Information");
                return false;

            }

            return true;
        }

           /// <summary>
        /// Configure Device using CarIQ settings (Settings from the CarIQTRConfig xml) - for server, apn
        /// Query and Write data of deviceid, imei, version, server, apn, timestamp into a csv file in the installation directory as configreport.csv
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button30_Click(object sender, EventArgs e)
        {
            String outfilePath = Application.StartupPath + "\\ConfigurationData.csv";

            // Logger Control
            RichTextBox LOGGER_TEXTBOX = rtbRecStr;

            // Controls and Buttons for Device Group
            Button DEVICEID_GROUP_SET_BUTTON = this.IDSendbutton;
            Button DEVICEID_GROUP_QUERY_BUTTON = this.button3;
            Control DEVICEID_TEXTBOX = IDSend;
            Control IMEI_LABEL = IMEI;
            Control VERSION_LABEL = VerID;
            
            // Device Id Group - to be queried only
            ConfigParamGroup deviceIdGroup = new ConfigParamGroup(DEVICEID_GROUP, null, DEVICEID_GROUP_SET_BUTTON, DEVICEID_GROUP_QUERY_BUTTON, LOGGER_TEXTBOX);
            deviceIdGroup.add(new ConfigParam(DEVICE_ID, IDSend, null, null, LOGGER_TEXTBOX)).
                add(new ConfigParam(IMEI_NUMBER, IMEI_LABEL, null, null, LOGGER_TEXTBOX)).
                add(new ConfigParam(VERSION_ID, VERSION_LABEL, null, null, LOGGER_TEXTBOX));



            // Controls and Buttons for Server Group
            Button SERVER_GROUP_SET_BUTTON = this.button8;
            Button SERVER_GROUP_QUERY_BUTTON = this.button9;
            Control SERVER_NAME_TEXTBOX = Mip;
            Control SERVER_PORT_TEXTBOX = Mport;
            Control APN_TEXTBOX = Mapn;
            ComboBox SERVER_CHOICE_BOX = ServerChoice;
 
            // Server Group - to be set and then queried
            ConfigParamGroup serverGroup = new ConfigParamGroup(SERVER_GROUP, SERVER_CHOICE_BOX, SERVER_GROUP_SET_BUTTON, SERVER_GROUP_QUERY_BUTTON, LOGGER_TEXTBOX);
            serverGroup.add(new ConfigParam(SERVER_NAME, SERVER_NAME_TEXTBOX, null, null, LOGGER_TEXTBOX)).
            add(new ConfigParam(SERVER_PORT, SERVER_PORT_TEXTBOX, null, null, LOGGER_TEXTBOX)).
            add(new ConfigParam(APN, APN_TEXTBOX, null, null, LOGGER_TEXTBOX));

             // iterate over params and write out
            StringBuilder builder = new StringBuilder();
            String[] parameters = {DEVICE_ID, IMEI_NUMBER, VERSION_ID, SERVER_NAME, SERVER_PORT, APN };
            foreach (var param in parameters)
            {
                if (deviceIdGroup.Contains(param))
                {
                    String paramValue = deviceIdGroup.getParamValue(param, true);
                    builder.Append(paramValue);
                    if (!check(param, paramValue))
                        return;
                }
                else if (serverGroup.Contains(param))
                {
                    String paramValue = serverGroup.getParamValue(param, true);
                    builder.Append(paramValue);
                    if (!check(param, paramValue))
                        return;
                }

                builder.Append(",");
            }

            builder.Append("\n");

            Console.Write(builder.ToString());
            File.AppendAllText(outfilePath, builder.ToString());
            MessageBox.Show("OK!\nResults saved to File: " + outfilePath);
            deviceIdGroup.resetDisplay();
            serverGroup.resetDisplay();
        }

        private void log(String str)
        {
            Console.WriteLine("\n" + DateTime.Now + " - " + Thread.CurrentThread.ManagedThreadId + " - " + str);
            // rtbRecStr.AppendText("\n" + DateTime.Now + " - " + Thread.CurrentThread.Name + " - " + str);
            CreateLogFile(str);
        }

        private void CreateLogFile(string Message)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "cqtrconfig.log";
            string logMessage = DateTime.Now.ToString() + ",TH-" + Thread.CurrentThread.ManagedThreadId.ToString() + "," + this.GetType().MemberType + "," + Message + Environment.NewLine;
            if (File.Exists(filepath))
            {
                File.AppendAllText(filepath, logMessage);
            }
            else
            {
                // Create the file.
                using (FileStream fs = File.Create(filepath))
                {
                    Byte[] info = new UTF8Encoding(true).GetBytes(logMessage);
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                }
            }

        }

        private string byteToString(byte[] data)
        {
            return BitConverter.ToString(data);
        }

        private string byteToString(byte data)
        {
            return string.Format("{0:x2}", data);
        }

        //private byte[] stringToByte(string str)
        //{

        //}

        public  string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public  byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
    /// <summary>
    /// Abstraction on top of underlying View-Model Mess to do the necessary
    /// </summary>
    class ConfigParam
    {
        String name;
        RichTextBox loggerBox;
        Control paramDisplayBox;
        Button paramQueryButton;
        Button paramSetButton;

        bool isSetIntoDevice = false;
        bool isQueriedFromDevice = false;

        /// <summary>
        /// Create config param
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="setButton"></param>
        /// <param name="queryButton"></param>
        public ConfigParam(String name, Control textBox, Button setButton, Button queryButton, RichTextBox loggerBox)
        {
            this.name = name;
            paramDisplayBox = textBox;
            paramSetButton = setButton;
            paramQueryButton = queryButton;
            this.loggerBox = loggerBox;
            reset();
        }

        public void reset()
        {
            isQueriedFromDevice = false;
            isSetIntoDevice = false;
        }

        private void log(String str)
        {
            Console.WriteLine("\n" + DateTime.Now + " - " + Thread.CurrentThread.ManagedThreadId + " - " + str);
            loggerBox.AppendText("\n" + DateTime.Now + " - " + Thread.CurrentThread.Name + " - " + str);
         }
        /// <summary>
        /// set the Value through the button
        /// </summary>
        /// <param name="text"></param>
        public void setValue(String text)
        {
            paramDisplayBox.Text = text;

            if (paramSetButton != null) {
                log("Setting Value:" + text + " to parameter: " + name);

                paramSetButton.PerformClick();

                //Thread.Sleep(100);
                isSetIntoDevice = true;
                isQueriedFromDevice = false;
            }
        }

        /// <summary>
        /// Query value by clicking query button
        /// </summary>
        /// <returns></returns>
        public String getValue(bool withoutQuery = false)
        {
             log("Querying Value from parameter: " + name);

            if (paramQueryButton != null)
            {
                paramQueryButton.PerformClick();

                // sleep a few miliseconds
                //Thread.Sleep(100);
                isQueriedFromDevice = true;
                isSetIntoDevice = false;
            }

            if (isQueriedFromDevice || withoutQuery)
                return paramDisplayBox.Text;

            return null; // not queried yet
        }

        public String Name { get { return name; } }

        // express using Property format
        public String Value { get { return getValue(); } set { setValue(value); } }
        public bool IsSet { get { return isSetIntoDevice; } set { isSetIntoDevice = value; } }
        public bool IsQueried { get { return isQueriedFromDevice; } set { isQueriedFromDevice = value; } }

        internal void resetDisplay()
        {
            paramDisplayBox.Text = "";
        }
    }

    /// <summary>
    /// Abstraction on top of Config Params... when all can be set and get
    /// </summary>
    class ConfigParamGroup
    {
        String name;
        RichTextBox loggerBox;
        Button paramQueryButton;
        Button paramSetButton;
        ComboBox contextCombo;
        Dictionary<String, ConfigParam> configParams = new Dictionary<string, ConfigParam>();

        /// <summary>
        /// Create config param
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="setButton"></param>
        /// <param name="queryButton"></param>
        public ConfigParamGroup(String name, ComboBox contextCombo, Button setButton, Button queryButton, RichTextBox loggerBox)
        {
            this.name = name;
            paramSetButton = setButton;
            paramQueryButton = queryButton;
            this.loggerBox = loggerBox;
            this.contextCombo = contextCombo;
        }

        public bool Contains(String name)
        {
            return configParams.ContainsKey(name);
        }

        private void log(String str)
        {
            Console.WriteLine("\n" + DateTime.Now + " - " + Thread.CurrentThread.ManagedThreadId + " - " + str);
            loggerBox.AppendText("\n" + DateTime.Now + " - " + Thread.CurrentThread.Name + " - " + str);
        }

        /// <summary>
        /// set the Value through the button
        /// </summary>
        /// <param name="text"></param>
        public ConfigParamGroup setParamValue(String paramName, String value)
        {
            ConfigParam param = null;

            if (!configParams.TryGetValue(paramName, out param)) {
                MessageBox.Show("Parameter not found: " + paramName);
                return this;
            }

            param.Value = value;
            return this;
        }

        /// <summary>
        /// set the Value through the button
        /// </summary>
        /// <param name="text"></param>
        public String getParamValue(String paramName, bool withoutQuery = false)
        {
            ConfigParam param = null;

            if (!configParams.TryGetValue(paramName, out param))
            {
                MessageBox.Show("Parameter not found: " + paramName);
                return null;
            }

            // if param is already queried, return it
            if (param.IsQueried)
                return param.Value;

            if (withoutQuery)
                return param.getValue(withoutQuery);

            return null;
        }

        /// <summary>
        /// Query value by clicking query button
        /// </summary>
        /// <returns></returns>
        public void fetch(int index)
        {
            if (contextCombo != null)
                contextCombo.SelectedIndex = index;

            log("Clicked on fetch button for " + name);
            paramQueryButton.PerformClick();

            //Thread.Sleep(200);

            // set all parameters as queried
            foreach (ConfigParam param in configParams.Values)
            {
                param.IsQueried = true;
            }

            log("Querying Value from parameter Group: " + name);
        }


        /// <summary>
        /// Query value by clicking query button
        /// </summary>
        /// <returns></returns>
        public void set(int index)
        {
            if (contextCombo != null)
                contextCombo.SelectedIndex = index;

            log("Clicked on SET for Group " + name + " in thread: " + Thread.CurrentThread.Name);
            paramSetButton.PerformClick();

            // Thread.Sleep(200);
            // set all parameters as Set
            foreach (ConfigParam param in configParams.Values)
            {
                param.IsSet = true;
            }
        }

        public String Name { get { return name; } }

        public ConfigParamGroup add(ConfigParam param)
        {
            configParams.Add(param.Name, param);
            return this;
        }

        internal void resetDisplay()
        {
            foreach (ConfigParam param in configParams.Values)
            {
                param.resetDisplay();
            }
        }
    }
}
