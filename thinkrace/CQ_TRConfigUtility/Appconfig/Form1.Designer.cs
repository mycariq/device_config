﻿namespace Appconfig
{
    partial class Form1
    {
        
        
        
        private System.ComponentModel.IContainer components = null;

        
        
        
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        
        
        
        
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.com1 = new System.IO.Ports.SerialPort(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tbSendStr = new System.Windows.Forms.TextBox();
            this.rtbRecStr = new System.Windows.Forms.RichTextBox();
            this.btSend = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button30 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.cmbBoxSpeed = new System.Windows.Forms.ComboBox();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.Zhendong = new System.Windows.Forms.ComboBox();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.Youhao = new System.Windows.Forms.TextBox();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.button22 = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.Password = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.User = new System.Windows.Forms.TextBox();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.button19 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ReadObd = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.EncryptionMode = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.WifiSwitch = new System.Windows.Forms.ComboBox();
            this.WifiName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.WifiPassword = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.button14 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.button7 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.POWERMODE = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.GPSNUM = new System.Windows.Forms.TextBox();
            this.GSMNET = new System.Windows.Forms.TextBox();
            this.Rtc = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Gsenor = new System.Windows.Forms.TextBox();
            this.Flash = new System.Windows.Forms.TextBox();
            this.GsmModel = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.GpsModel = new System.Windows.Forms.TextBox();
            this.Kline = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.HighCan = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ServerChoice = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Mapn = new System.Windows.Forms.TextBox();
            this.Mport = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.Mip = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.IMEI = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.IDSendbutton = new System.Windows.Forms.Button();
            this.VerID = new System.Windows.Forms.Label();
            this.IDSend = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.com1Port = new System.Windows.Forms.ComboBox();
            this.btOpen = new System.Windows.Forms.Button();
            this.tabCtrlNew = new System.Windows.Forms.TabControl();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabCtrlNew.SuspendLayout();
            this.SuspendLayout();
            // 
            // com1
            // 
            this.com1.BaudRate = 115200;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage2.Controls.Add(this.tbSendStr);
            this.tabPage2.Controls.Add(this.rtbRecStr);
            this.tabPage2.Controls.Add(this.btSend);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(870, 495);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Debug window";
            // 
            // tbSendStr
            // 
            this.tbSendStr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSendStr.Location = new System.Drawing.Point(26, 15);
            this.tbSendStr.MaxLength = 2147483647;
            this.tbSendStr.Multiline = true;
            this.tbSendStr.Name = "tbSendStr";
            this.tbSendStr.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSendStr.Size = new System.Drawing.Size(647, 27);
            this.tbSendStr.TabIndex = 8;
            this.tbSendStr.Text = "123";
            // 
            // rtbRecStr
            // 
            this.rtbRecStr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbRecStr.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.rtbRecStr.HideSelection = false;
            this.rtbRecStr.Location = new System.Drawing.Point(20, 56);
            this.rtbRecStr.Margin = new System.Windows.Forms.Padding(2);
            this.rtbRecStr.Name = "rtbRecStr";
            this.rtbRecStr.Size = new System.Drawing.Size(653, 321);
            this.rtbRecStr.TabIndex = 4;
            this.rtbRecStr.TabStop = false;
            this.rtbRecStr.Text = "";
            // 
            // btSend
            // 
            this.btSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSend.Font = new System.Drawing.Font("Microsoft YaHei", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btSend.ForeColor = System.Drawing.Color.ForestGreen;
            this.btSend.Location = new System.Drawing.Point(717, 8);
            this.btSend.Name = "btSend";
            this.btSend.Size = new System.Drawing.Size(86, 35);
            this.btSend.TabIndex = 7;
            this.btSend.Text = "Send";
            this.btSend.UseVisualStyleBackColor = true;
            this.btSend.Click += new System.EventHandler(this.btSend_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.button30);
            this.tabPage1.Controls.Add(this.button29);
            this.tabPage1.Controls.Add(this.groupBox12);
            this.tabPage1.Controls.Add(this.groupBox11);
            this.tabPage1.Controls.Add(this.groupBox10);
            this.tabPage1.Controls.Add(this.button22);
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Controls.Add(this.button19);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(870, 495);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Main";
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.DarkGreen;
            this.button30.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.button30.FlatAppearance.BorderSize = 5;
            this.button30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button30.ForeColor = System.Drawing.Color.White;
            this.button30.Location = new System.Drawing.Point(451, 448);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(419, 26);
            this.button30.TabIndex = 8;
            this.button30.Text = "Query";
            this.button30.UseVisualStyleBackColor = false;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.Maroon;
            this.button29.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.button29.FlatAppearance.BorderSize = 5;
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button29.ForeColor = System.Drawing.Color.White;
            this.button29.Location = new System.Drawing.Point(0, 448);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(419, 26);
            this.button29.TabIndex = 8;
            this.button29.Text = "Configure";
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.cmbBoxSpeed);
            this.groupBox12.Controls.Add(this.button27);
            this.groupBox12.Controls.Add(this.button28);
            this.groupBox12.Controls.Add(this.label26);
            this.groupBox12.Location = new System.Drawing.Point(499, 118);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(151, 74);
            this.groupBox12.TabIndex = 34;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Sharp acceleration";
            // 
            // cmbBoxSpeed
            // 
            this.cmbBoxSpeed.FormattingEnabled = true;
            this.cmbBoxSpeed.Items.AddRange(new object[] {
            "High",
            "Mid",
            "Low"});
            this.cmbBoxSpeed.Location = new System.Drawing.Point(96, 13);
            this.cmbBoxSpeed.Name = "cmbBoxSpeed";
            this.cmbBoxSpeed.Size = new System.Drawing.Size(55, 21);
            this.cmbBoxSpeed.TabIndex = 6;
            // 
            // button27
            // 
            this.button27.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button27.ForeColor = System.Drawing.Color.Black;
            this.button27.Location = new System.Drawing.Point(84, 43);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(54, 28);
            this.button27.TabIndex = 5;
            this.button27.Text = "Query";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button28
            // 
            this.button28.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button28.ForeColor = System.Drawing.Color.Black;
            this.button28.Location = new System.Drawing.Point(6, 41);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(50, 28);
            this.button28.TabIndex = 4;
            this.button28.Text = "Set";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(6, 19);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(95, 14);
            this.label26.TabIndex = 0;
            this.label26.Text = "Sensitivity";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.Zhendong);
            this.groupBox11.Controls.Add(this.button25);
            this.groupBox11.Controls.Add(this.button26);
            this.groupBox11.Controls.Add(this.label25);
            this.groupBox11.Location = new System.Drawing.Point(721, 7);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(143, 111);
            this.groupBox11.TabIndex = 33;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Vibrtion level";
            // 
            // Zhendong
            // 
            this.Zhendong.FormattingEnabled = true;
            this.Zhendong.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30"});
            this.Zhendong.Location = new System.Drawing.Point(88, 28);
            this.Zhendong.Name = "Zhendong";
            this.Zhendong.Size = new System.Drawing.Size(46, 21);
            this.Zhendong.TabIndex = 6;
            // 
            // button25
            // 
            this.button25.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button25.ForeColor = System.Drawing.Color.Black;
            this.button25.Location = new System.Drawing.Point(80, 66);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(54, 38);
            this.button25.TabIndex = 5;
            this.button25.Text = "Query";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button26
            // 
            this.button26.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button26.ForeColor = System.Drawing.Color.Black;
            this.button26.Location = new System.Drawing.Point(6, 66);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(54, 38);
            this.button26.TabIndex = 4;
            this.button26.Text = "Set";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(15, 33);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(47, 14);
            this.label25.TabIndex = 0;
            this.label25.Text = "level";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.Youhao);
            this.groupBox10.Controls.Add(this.button23);
            this.groupBox10.Controls.Add(this.button24);
            this.groupBox10.Controls.Add(this.label24);
            this.groupBox10.Location = new System.Drawing.Point(495, 284);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(153, 85);
            this.groupBox10.TabIndex = 28;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "AVG Fuel consumption";
            // 
            // Youhao
            // 
            this.Youhao.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Youhao.Location = new System.Drawing.Point(70, 19);
            this.Youhao.Name = "Youhao";
            this.Youhao.Size = new System.Drawing.Size(68, 23);
            this.Youhao.TabIndex = 12;
            // 
            // button23
            // 
            this.button23.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button23.ForeColor = System.Drawing.Color.Black;
            this.button23.Location = new System.Drawing.Point(85, 49);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(54, 28);
            this.button23.TabIndex = 5;
            this.button23.Text = "Query";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button24
            // 
            this.button24.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button24.ForeColor = System.Drawing.Color.Black;
            this.button24.Location = new System.Drawing.Point(6, 49);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(50, 28);
            this.button24.TabIndex = 4;
            this.button24.Text = "Set";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(6, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 14);
            this.label24.TabIndex = 0;
            this.label24.Text = "AVG";
            // 
            // button22
            // 
            this.button22.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button22.ForeColor = System.Drawing.Color.Black;
            this.button22.Location = new System.Drawing.Point(552, 368);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(98, 38);
            this.button22.TabIndex = 32;
            this.button22.Text = "Restart";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.Password);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.User);
            this.groupBox9.Controls.Add(this.button20);
            this.groupBox9.Controls.Add(this.button21);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Location = new System.Drawing.Point(16, 340);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(212, 112);
            this.groupBox9.TabIndex = 31;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "APN Setting";
            // 
            // Password
            // 
            this.Password.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Password.Location = new System.Drawing.Point(83, 50);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(123, 23);
            this.Password.TabIndex = 8;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(6, 54);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 14);
            this.label22.TabIndex = 7;
            this.label22.Text = "Password";
            // 
            // User
            // 
            this.User.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.User.Location = new System.Drawing.Point(83, 18);
            this.User.Name = "User";
            this.User.Size = new System.Drawing.Size(123, 23);
            this.User.TabIndex = 6;
            // 
            // button20
            // 
            this.button20.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button20.ForeColor = System.Drawing.Color.Black;
            this.button20.Location = new System.Drawing.Point(125, 75);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(67, 28);
            this.button20.TabIndex = 5;
            this.button20.Text = "Query";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button21.ForeColor = System.Drawing.Color.Black;
            this.button21.Location = new System.Drawing.Point(9, 75);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(67, 28);
            this.button21.TabIndex = 4;
            this.button21.Text = "Set";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(6, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 14);
            this.label21.TabIndex = 0;
            this.label21.Text = "User name";
            // 
            // button19
            // 
            this.button19.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button19.ForeColor = System.Drawing.Color.Black;
            this.button19.Location = new System.Drawing.Point(552, 408);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(98, 38);
            this.button19.TabIndex = 30;
            this.button19.Text = "Restore fact ory settings";
            this.button19.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.ReadObd);
            this.groupBox6.Controls.Add(this.button1);
            this.groupBox6.Controls.Add(this.button4);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Location = new System.Drawing.Point(495, 198);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(153, 80);
            this.groupBox6.TabIndex = 27;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "OBD data reading";
            // 
            // ReadObd
            // 
            this.ReadObd.FormattingEnabled = true;
            this.ReadObd.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.ReadObd.Location = new System.Drawing.Point(79, 21);
            this.ReadObd.Name = "ReadObd";
            this.ReadObd.Size = new System.Drawing.Size(60, 21);
            this.ReadObd.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(86, 49);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 28);
            this.button1.TabIndex = 5;
            this.button1.Text = "Query";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(6, 49);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 28);
            this.button4.TabIndex = 4;
            this.button4.Text = "Set";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Read OBD";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button11);
            this.groupBox4.Controls.Add(this.button10);
            this.groupBox4.Controls.Add(this.EncryptionMode);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.button18);
            this.groupBox4.Controls.Add(this.button17);
            this.groupBox4.Controls.Add(this.button16);
            this.groupBox4.Controls.Add(this.button15);
            this.groupBox4.Controls.Add(this.WifiSwitch);
            this.groupBox4.Controls.Add(this.WifiName);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.WifiPassword);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Location = new System.Drawing.Point(229, 138);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(264, 199);
            this.groupBox4.TabIndex = 29;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "WIFI funtion";
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button11.ForeColor = System.Drawing.Color.Black;
            this.button11.Location = new System.Drawing.Point(206, 154);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(54, 31);
            this.button11.TabIndex = 20;
            this.button11.Text = "Query";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button10.ForeColor = System.Drawing.Color.Black;
            this.button10.Location = new System.Drawing.Point(157, 154);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(50, 31);
            this.button10.TabIndex = 19;
            this.button10.Text = "Set";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // EncryptionMode
            // 
            this.EncryptionMode.FormattingEnabled = true;
            this.EncryptionMode.Items.AddRange(new object[] {
            "WPA_TKIP",
            "WPA2-PSK"});
            this.EncryptionMode.Location = new System.Drawing.Point(81, 160);
            this.EncryptionMode.Name = "EncryptionMode";
            this.EncryptionMode.Size = new System.Drawing.Size(70, 21);
            this.EncryptionMode.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(7, 164);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 14);
            this.label11.TabIndex = 17;
            this.label11.Text = "Encryption";
            // 
            // button18
            // 
            this.button18.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button18.ForeColor = System.Drawing.Color.Black;
            this.button18.Location = new System.Drawing.Point(145, 93);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(67, 28);
            this.button18.TabIndex = 16;
            this.button18.Tag = "0";
            this.button18.Text = "Query";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button17.ForeColor = System.Drawing.Color.Black;
            this.button17.Location = new System.Drawing.Point(37, 93);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(67, 28);
            this.button17.TabIndex = 15;
            this.button17.Tag = "0";
            this.button17.Text = "Set";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button16.ForeColor = System.Drawing.Color.Black;
            this.button16.Location = new System.Drawing.Point(196, 17);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(54, 31);
            this.button16.TabIndex = 14;
            this.button16.Text = "Query";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button15.ForeColor = System.Drawing.Color.Black;
            this.button15.Location = new System.Drawing.Point(140, 17);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(50, 31);
            this.button15.TabIndex = 13;
            this.button15.Text = "Set";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // WifiSwitch
            // 
            this.WifiSwitch.FormattingEnabled = true;
            this.WifiSwitch.Items.AddRange(new object[] {
            "ON",
            "OFF"});
            this.WifiSwitch.Location = new System.Drawing.Point(81, 26);
            this.WifiSwitch.Name = "WifiSwitch";
            this.WifiSwitch.Size = new System.Drawing.Size(53, 21);
            this.WifiSwitch.TabIndex = 12;
            // 
            // WifiName
            // 
            this.WifiName.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.WifiName.Location = new System.Drawing.Point(83, 62);
            this.WifiName.Name = "WifiName";
            this.WifiName.Size = new System.Drawing.Size(143, 23);
            this.WifiName.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(6, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 14);
            this.label9.TabIndex = 10;
            this.label9.Text = "WIFI Name";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(7, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 14);
            this.label10.TabIndex = 6;
            this.label10.Text = "WIFI SW";
            // 
            // WifiPassword
            // 
            this.WifiPassword.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.WifiPassword.Location = new System.Drawing.Point(83, 128);
            this.WifiPassword.Name = "WifiPassword";
            this.WifiPassword.Size = new System.Drawing.Size(143, 23);
            this.WifiPassword.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(8, 131);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(71, 14);
            this.label20.TabIndex = 1;
            this.label20.Text = "WIFI Psw";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.button14);
            this.groupBox8.Controls.Add(this.progressBar1);
            this.groupBox8.Controls.Add(this.button7);
            this.groupBox8.Controls.Add(this.button12);
            this.groupBox8.Location = new System.Drawing.Point(234, 365);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(312, 87);
            this.groupBox8.TabIndex = 28;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Upgrade";
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button14.ForeColor = System.Drawing.Color.Black;
            this.button14.Location = new System.Drawing.Point(164, 49);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(142, 28);
            this.button14.TabIndex = 7;
            this.button14.Text = "Serial Upgrade";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(93, 14);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(202, 28);
            this.progressBar1.TabIndex = 6;
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button7.ForeColor = System.Drawing.Color.Black;
            this.button7.Location = new System.Drawing.Point(6, 14);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(81, 28);
            this.button7.TabIndex = 5;
            this.button7.Text = "Select file";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button12.ForeColor = System.Drawing.Color.Black;
            this.button12.Location = new System.Drawing.Point(6, 49);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(140, 28);
            this.button12.TabIndex = 4;
            this.button12.Text = "K Line Upgrade";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.POWERMODE);
            this.groupBox7.Controls.Add(this.button5);
            this.groupBox7.Controls.Add(this.button6);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Location = new System.Drawing.Point(566, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(153, 114);
            this.groupBox7.TabIndex = 26;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Power saving mode";
            // 
            // POWERMODE
            // 
            this.POWERMODE.FormattingEnabled = true;
            this.POWERMODE.Items.AddRange(new object[] {
            "ON",
            "OFF"});
            this.POWERMODE.Location = new System.Drawing.Point(88, 28);
            this.POWERMODE.Name = "POWERMODE";
            this.POWERMODE.Size = new System.Drawing.Size(46, 21);
            this.POWERMODE.TabIndex = 6;
            this.POWERMODE.SelectedIndexChanged += new System.EventHandler(this.POWERMODE_SelectedIndexChanged);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(88, 70);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(54, 38);
            this.button5.TabIndex = 5;
            this.button5.Text = "Query";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.Location = new System.Drawing.Point(6, 70);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(50, 38);
            this.button6.TabIndex = 4;
            this.button6.Text = "Set";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(15, 33);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 14);
            this.label23.TabIndex = 0;
            this.label23.Text = "mode";
            this.label23.Click += new System.EventHandler(this.label23_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.GPSNUM);
            this.groupBox5.Controls.Add(this.GSMNET);
            this.groupBox5.Controls.Add(this.Rtc);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.Gsenor);
            this.groupBox5.Controls.Add(this.Flash);
            this.groupBox5.Controls.Add(this.GsmModel);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.GpsModel);
            this.groupBox5.Controls.Add(this.Kline);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.button13);
            this.groupBox5.Controls.Add(this.HighCan);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Location = new System.Drawing.Point(656, 137);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(211, 311);
            this.groupBox5.TabIndex = 24;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Device test";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(50, 275);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(127, 30);
            this.button2.TabIndex = 20;
            this.button2.Tag = "0";
            this.button2.Text = "Display Clear";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // GPSNUM
            // 
            this.GPSNUM.Location = new System.Drawing.Point(163, 89);
            this.GPSNUM.Name = "GPSNUM";
            this.GPSNUM.Size = new System.Drawing.Size(36, 20);
            this.GPSNUM.TabIndex = 19;
            // 
            // GSMNET
            // 
            this.GSMNET.Location = new System.Drawing.Point(163, 124);
            this.GSMNET.Name = "GSMNET";
            this.GSMNET.Size = new System.Drawing.Size(36, 20);
            this.GSMNET.TabIndex = 18;
            // 
            // Rtc
            // 
            this.Rtc.Location = new System.Drawing.Point(101, 220);
            this.Rtc.Name = "Rtc";
            this.Rtc.Size = new System.Drawing.Size(98, 20);
            this.Rtc.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(33, 223);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 12);
            this.label5.TabIndex = 16;
            this.label5.Text = "RTC Clock";
            // 
            // Gsenor
            // 
            this.Gsenor.Location = new System.Drawing.Point(101, 184);
            this.Gsenor.Name = "Gsenor";
            this.Gsenor.Size = new System.Drawing.Size(98, 20);
            this.Gsenor.TabIndex = 15;
            // 
            // Flash
            // 
            this.Flash.Location = new System.Drawing.Point(101, 154);
            this.Flash.Name = "Flash";
            this.Flash.Size = new System.Drawing.Size(98, 20);
            this.Flash.TabIndex = 14;
            // 
            // GsmModel
            // 
            this.GsmModel.Location = new System.Drawing.Point(101, 124);
            this.GsmModel.Name = "GsmModel";
            this.GsmModel.Size = new System.Drawing.Size(56, 20);
            this.GsmModel.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(28, 194);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 12);
            this.label17.TabIndex = 12;
            this.label17.Text = "Gsensor";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(28, 157);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 12);
            this.label16.TabIndex = 11;
            this.label16.Text = "Mem chip";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(28, 127);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 12);
            this.label15.TabIndex = 10;
            this.label15.Text = "GSM Mdl";
            // 
            // GpsModel
            // 
            this.GpsModel.Location = new System.Drawing.Point(101, 89);
            this.GpsModel.Name = "GpsModel";
            this.GpsModel.Size = new System.Drawing.Size(56, 20);
            this.GpsModel.TabIndex = 9;
            // 
            // Kline
            // 
            this.Kline.Location = new System.Drawing.Point(101, 56);
            this.Kline.Name = "Kline";
            this.Kline.Size = new System.Drawing.Size(98, 20);
            this.Kline.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(28, 59);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 12);
            this.label12.TabIndex = 6;
            this.label12.Text = "K Line";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(28, 89);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 12);
            this.label13.TabIndex = 5;
            this.label13.Text = "GPS Mdl";
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button13.ForeColor = System.Drawing.Color.Black;
            this.button13.Location = new System.Drawing.Point(48, 245);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(128, 30);
            this.button13.TabIndex = 4;
            this.button13.Tag = "0";
            this.button13.Text = "Start test";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // HighCan
            // 
            this.HighCan.Location = new System.Drawing.Point(101, 24);
            this.HighCan.Name = "HighCan";
            this.HighCan.Size = new System.Drawing.Size(98, 20);
            this.HighCan.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(28, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 12);
            this.label14.TabIndex = 1;
            this.label14.Text = "CAN";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ServerChoice);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.Mapn);
            this.groupBox3.Controls.Add(this.Mport);
            this.groupBox3.Controls.Add(this.button9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.button8);
            this.groupBox3.Controls.Add(this.Mip);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(16, 139);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(212, 199);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Main Server";
            // 
            // ServerChoice
            // 
            this.ServerChoice.FormattingEnabled = true;
            this.ServerChoice.Items.AddRange(new object[] {
            "Main Server",
            "standby server"});
            this.ServerChoice.Location = new System.Drawing.Point(64, 26);
            this.ServerChoice.Name = "ServerChoice";
            this.ServerChoice.Size = new System.Drawing.Size(95, 21);
            this.ServerChoice.TabIndex = 11;
            this.ServerChoice.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(6, 27);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 14);
            this.label19.TabIndex = 10;
            this.label19.Text = "Server";
            // 
            // Mapn
            // 
            this.Mapn.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Mapn.Location = new System.Drawing.Point(64, 122);
            this.Mapn.Name = "Mapn";
            this.Mapn.Size = new System.Drawing.Size(143, 23);
            this.Mapn.TabIndex = 9;
            this.Mapn.Text = "CMNET";
            // 
            // Mport
            // 
            this.Mport.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Mport.Location = new System.Drawing.Point(64, 93);
            this.Mport.Name = "Mport";
            this.Mport.Size = new System.Drawing.Size(143, 23);
            this.Mport.TabIndex = 8;
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button9.ForeColor = System.Drawing.Color.Black;
            this.button9.Location = new System.Drawing.Point(125, 158);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(67, 28);
            this.button9.TabIndex = 7;
            this.button9.Tag = "0";
            this.button9.Text = "Query";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(17, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 14);
            this.label8.TabIndex = 6;
            this.label8.Text = "Port";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(17, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 14);
            this.label7.TabIndex = 5;
            this.label7.Text = "APN";
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button8.ForeColor = System.Drawing.Color.Black;
            this.button8.Location = new System.Drawing.Point(9, 157);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(67, 28);
            this.button8.TabIndex = 4;
            this.button8.Tag = "0";
            this.button8.Text = "Set";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // Mip
            // 
            this.Mip.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Mip.Location = new System.Drawing.Point(64, 62);
            this.Mip.Name = "Mip";
            this.Mip.Size = new System.Drawing.Size(143, 23);
            this.Mip.TabIndex = 3;
            this.Mip.TextChanged += new System.EventHandler(this.Mip_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(3, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 14);
            this.label6.TabIndex = 1;
            this.label6.Text = "IP/DN";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.IMEI);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.IDSendbutton);
            this.groupBox2.Controls.Add(this.VerID);
            this.groupBox2.Controls.Add(this.IDSend);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(167, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(391, 114);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Basic information";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // IMEI
            // 
            this.IMEI.AutoSize = true;
            this.IMEI.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IMEI.ForeColor = System.Drawing.Color.Black;
            this.IMEI.Location = new System.Drawing.Point(96, 61);
            this.IMEI.Name = "IMEI";
            this.IMEI.Size = new System.Drawing.Size(63, 14);
            this.IMEI.TabIndex = 7;
            this.IMEI.Text = "xxxxxxx";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(22, 61);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 14);
            this.label18.TabIndex = 6;
            this.label18.Text = "IMEI";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkGreen;
            this.button3.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(298, 66);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 38);
            this.button3.TabIndex = 5;
            this.button3.Text = "Refresh";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // IDSendbutton
            // 
            this.IDSendbutton.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IDSendbutton.ForeColor = System.Drawing.Color.Black;
            this.IDSendbutton.Location = new System.Drawing.Point(298, 19);
            this.IDSendbutton.Name = "IDSendbutton";
            this.IDSendbutton.Size = new System.Drawing.Size(75, 38);
            this.IDSendbutton.TabIndex = 4;
            this.IDSendbutton.Text = "set";
            this.IDSendbutton.UseVisualStyleBackColor = true;
            this.IDSendbutton.Click += new System.EventHandler(this.IDSendbutton_Click);
            // 
            // VerID
            // 
            this.VerID.AutoSize = true;
            this.VerID.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.VerID.ForeColor = System.Drawing.Color.Black;
            this.VerID.Location = new System.Drawing.Point(96, 89);
            this.VerID.Name = "VerID";
            this.VerID.Size = new System.Drawing.Size(63, 14);
            this.VerID.TabIndex = 3;
            this.VerID.Text = "xxxxxxx";
            // 
            // IDSend
            // 
            this.IDSend.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IDSend.Location = new System.Drawing.Point(120, 24);
            this.IDSend.Name = "IDSend";
            this.IDSend.Size = new System.Drawing.Size(117, 23);
            this.IDSend.TabIndex = 2;
            this.IDSend.Text = "601400000000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(22, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 14);
            this.label4.TabIndex = 1;
            this.label4.Text = "Ver.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(38, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "ID";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.com1Port);
            this.groupBox1.Controls.Add(this.btOpen);
            this.groupBox1.Location = new System.Drawing.Point(7, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(154, 114);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serial config";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "serial";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // com1Port
            // 
            this.com1Port.FormattingEnabled = true;
            this.com1Port.Location = new System.Drawing.Point(70, 26);
            this.com1Port.Name = "com1Port";
            this.com1Port.Size = new System.Drawing.Size(73, 21);
            this.com1Port.TabIndex = 1;
            this.com1Port.SelectedIndexChanged += new System.EventHandler(this.com1Port_SelectedIndexChanged);
            // 
            // btOpen
            // 
            this.btOpen.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btOpen.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btOpen.Location = new System.Drawing.Point(36, 66);
            this.btOpen.Name = "btOpen";
            this.btOpen.Size = new System.Drawing.Size(107, 38);
            this.btOpen.TabIndex = 3;
            this.btOpen.Text = "Open port";
            this.btOpen.UseVisualStyleBackColor = true;
            this.btOpen.Click += new System.EventHandler(this.btOpen_Click_1);
            // 
            // tabCtrlNew
            // 
            this.tabCtrlNew.Controls.Add(this.tabPage1);
            this.tabCtrlNew.Controls.Add(this.tabPage2);
            this.tabCtrlNew.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tabCtrlNew.Location = new System.Drawing.Point(1, -3);
            this.tabCtrlNew.Name = "tabCtrlNew";
            this.tabCtrlNew.SelectedIndex = 0;
            this.tabCtrlNew.Size = new System.Drawing.Size(878, 521);
            this.tabCtrlNew.TabIndex = 19;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(883, 496);
            this.Controls.Add(this.tabCtrlNew);
            this.ForeColor = System.Drawing.Color.MediumBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "VT200 Configuration Software V1.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabCtrlNew.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.IO.Ports.SerialPort com1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox tbSendStr;
        private System.Windows.Forms.RichTextBox rtbRecStr;
        private System.Windows.Forms.Button btSend;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.ComboBox cmbBoxSpeed;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox Zhendong;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox Youhao;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox User;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox ReadObd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.ComboBox EncryptionMode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.ComboBox WifiSwitch;
        private System.Windows.Forms.TextBox WifiName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox WifiPassword;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox POWERMODE;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox GPSNUM;
        private System.Windows.Forms.TextBox GSMNET;
        private System.Windows.Forms.TextBox Rtc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Gsenor;
        private System.Windows.Forms.TextBox Flash;
        private System.Windows.Forms.TextBox GsmModel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox GpsModel;
        private System.Windows.Forms.TextBox Kline;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.TextBox HighCan;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox ServerChoice;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Mapn;
        private System.Windows.Forms.TextBox Mport;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox Mip;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label IMEI;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button IDSendbutton;
        private System.Windows.Forms.Label VerID;
        private System.Windows.Forms.TextBox IDSend;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox com1Port;
        private System.Windows.Forms.Button btOpen;
        private System.Windows.Forms.TabControl tabCtrlNew;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
    }
}

