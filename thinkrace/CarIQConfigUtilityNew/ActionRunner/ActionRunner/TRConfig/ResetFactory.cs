﻿using Runner;
using System.Collections.Generic;

namespace TRConfig
{
    class ResetFactory : TRConfigActor
    {
        static string[] requests = { "2800000000000000040001010429" };
        static string[] commandNames = { "Reset Factory" };

        protected override int CommandCount
        {
            get
            {
                return requests.Length;
            }
        }

        protected override Command getNextCommand(int index, Command previous, ActionContext context, Dictionary<string, object> parameters)
        {
            if (index >= requests.Length)
                return null;

            return new Command(commandNames[index], WritePacket.CreateFromText(requests[index]));
        }
    }
}
