﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TRConfig
{
    public class WritePacket : Packet
    {
        private WritePacket(byte[] bytes)
            : base(bytes)
        {
        }

        internal static Packet CreateFromText(string defaulttext)
        {
            return new WritePacket(Packet.StringToByteArray(defaulttext));
        }

        internal static Packet CreateFromText(byte[] deviceId, byte type, byte subtype, string text)
        {
            List<byte> textArray = Encoding.Default.GetBytes(text).ToList();
            List<byte> rawBytess = new List<byte>();
            //Device ID 1-6
            if (deviceId != null)
                rawBytess.AddRange(TRUtil.getBytes(deviceId, 6));
            else
                rawBytess.AddRange(TRUtil.getNullBytes(6));

            //7-Type
            rawBytess.Add(type);

            //8-subtype
            rawBytess.Add(subtype);

            //9-textlength
            rawBytess.Add(0x00);

            //10- Textlength
            rawBytess.Add((byte)(1 + textArray.Count));
            rawBytess.Add(0x01);
            //11-text
            rawBytess.AddRange(textArray);

            //Checksum
            //Byte _CheckSumByte = 0x00;
            //for (int index = 0; index < rawBytess.Count; index++)
            //    _CheckSumByte ^= rawBytess[index];

            rawBytess.Add(CalCheckSum(rawBytess.ToArray()));

            List<byte> sendData = new List<byte>();
            //Add START
            sendData.Add(START);
            //Adding Escape Sequence
            for (int index = 0; index < rawBytess.Count; index++)
            {
                if (rawBytess[index] == START || rawBytess[index] == END || rawBytess[index] == ESCAPE)
                {
                    sendData.Add(ESCAPE);
                    sendData.Add((byte)(ESCAPE ^ rawBytess[index]));
                }
                else
                {
                    sendData.Add(rawBytess[index]);
                }
            }

            //Add END 
            sendData.Add(END);

            // Now actually construct the packet
            Packet retval = new WritePacket(sendData.ToArray());
            if (retval.Validate != PacketStatus.Valid)
                throw new Exception("Packet is invalid!!! " + retval.ToRawByteString());

            return retval;
        }

        internal static Packet CreateFromText(byte type, byte subtype, string text)
        {
            return CreateFromText(null, type, subtype, text);
        }

        public static Packet CreateFromBytes(byte[] deviceId, byte type, byte subtype, byte[] bytes)
        {
            List<byte> rawBytess = new List<byte>();
            //Device ID 1-6
            if (deviceId != null)
                rawBytess.AddRange(TRUtil.getBytes(deviceId, 6));
            else
                rawBytess.AddRange(TRUtil.getNullBytes(6));

            //7-Type
            rawBytess.Add(type);

            //8-subtype
            rawBytess.Add(subtype);

            //reamining byte
            rawBytess.AddRange(bytes);

            //Checksum
            //Byte _CheckSumByte = 0x00;
            //for (int index = 0; index < rawBytess.Count; index++)
            //    _CheckSumByte ^= rawBytess[index];

            rawBytess.Add(CalCheckSum(rawBytess.ToArray()));

            List<byte> sendData = new List<byte>();
            //Add START
            sendData.Add(START);
            //Adding Escape Sequence
            for (int index = 0; index < rawBytess.Count; index++)
            {
                if (rawBytess[index] == START || rawBytess[index] == END || rawBytess[index] == ESCAPE)
                {
                    sendData.Add(ESCAPE);
                    sendData.Add((byte)(ESCAPE ^ rawBytess[index]));
                }
                else
                {
                    sendData.Add(rawBytess[index]);
                }
            }

            //Add END 
            sendData.Add(END);

            // Now actually construct the packet
            Packet retval = new WritePacket(sendData.ToArray());
            if (retval.Validate != PacketStatus.Valid)
                throw new Exception("Packet is invalid!!! " + retval.ToRawByteString());

            return retval;

        }

        public static Packet CreateFromBytes(byte type, byte subtype, byte[] bytes)
        {
            return CreateFromBytes(null, type, subtype, bytes);
        }
    }
}