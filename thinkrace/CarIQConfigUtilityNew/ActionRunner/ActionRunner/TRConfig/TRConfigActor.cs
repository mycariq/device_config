﻿using Runner;
using System;
using System.Collections.Generic;

namespace TRConfig
{
    abstract class TRConfigActor : Actor
    {
        protected abstract Command getNextCommand(int index, Command previous, ActionContext context, Dictionary<string, object> parameters);
        protected abstract int CommandCount { get; }

        protected virtual int BaudRate { get { return TRUtil.DEFAULT_BAUD_RATE; } }

        protected override void run(ActionContext context, Dictionary<string, object> parameters)
        {
            //context.Log(this.GetType().Name, "I am " + this.GetType().FullName);
            string comPortName = context.GetParameter<string>(TRUtil.COMPORT);

            using (ComPort port = ComPort.Open(comPortName, BaudRate, context))
            {
                // get the next command from derived class by passing the previous
                int indx = 0;
                context.Report("TRConfigActor", "=========================\nStart Commands...");

                Command cmd = getNextCommand(indx++, null, context, parameters);
                while (cmd != null)
                {
                    context.SetParameter(TRUtil.COMMAND, cmd);

                    context.Report("TRConfigActor", "Executing Command " + indx + " out of " + CommandCount + " : " + cmd.Name);
                    cmd.execute(context, port);

                    string commandResponse = cmd.Response == null ? "No Response!" : cmd.Response.ToString();
                    string rawResponse = cmd.Response == null ? "No Response!" : cmd.Response.ToRawByteString();

                    //      context.Log(this.GetType().Name, this.GetType().Name + " Command Response: " + commandResponse);
                    context.Log(this.GetType().Name, this.GetType().Name + " Command Raw Response: " + rawResponse);


                    string cmdReport = cmd.Response == null ? "No Response!!" : "DeviceID:" + cmd.Response.DeviceID + ",Text:" + cmd.Response.Text;
                    context.Report(this.GetType().Name, cmdReport);
                    ////<PARAM> 
                    //context.CSVReport(this.GetType().Name + "," + indx + "," + (cmd.Response == null ? "No Response!" : cmd.Response.DeviceID + "," + cmd.Response.Text));

                    // send current command as previous and fetch next
                    try
                    {
                        cmd = getNextCommand(indx++, cmd, context, parameters);
                    }
                    catch (ComPortResetException e)
                    {
                        context.Log("ComPort", "Reseting Comport!!");
                        port.Reset();
                    }

                }

                context.Report("TRConfigActor", "+++++++++++++++++++++++\n Commands Executed Successfully\n+++++++++++++++++++++");
            }
        }
    }
}
