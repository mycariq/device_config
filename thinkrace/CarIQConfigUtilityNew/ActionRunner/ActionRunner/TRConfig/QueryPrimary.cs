﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Runner;

namespace TRConfig
{
    class QueryPrimary : TRConfigActor
    {
        static string[] requests = { "2800000000000010010001001029", "2800000000000010020001001329" };
        static string[] commandNames = { "Query Primary Server", "Query Secondary Server" };

        protected override Command getNextCommand(int index, Command previous, ActionContext context, Dictionary<string, object> parameters)
        {

            //save results in context for csv file
            if (previous != null && previous.Response != null)
            {
                string[] results = previous.Response.Text.Split(',');

                if (results.Length >= 2)
                {
                    context.SetParameter(TRUtil.DEVICE_ID, previous.Response.DeviceID);
                    context.SetParameter(TRUtil.SERVER, results[0]);
                    context.SetParameter(TRUtil.PORT, results[1]);
                    context.SetParameter(TRUtil.APN, results[2]);
                }
            }


            if (index >= requests.Length)
                return null;

            return new Command(commandNames[index], WritePacket.CreateFromText(requests[index]));
        }

        protected override int CommandCount
        {
            get
            {
                return requests.Length;
            }
        }
    }
}
