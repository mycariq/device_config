﻿using Runner;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TRConfig
{
    class FirmwareUpgrade : TRConfigActor
    {

        bool isInitialised = false;

        List<Packet> packetList = new List<Packet>();

        protected override int BaudRate { get { return TRUtil.DEFAULT_BAUD_RATE; } }

        private void Initialise(Dictionary<string, object> parameters, ActionContext context)
        {

            // getting filepath and version name of the firmware file
            string filePath = Runner.Util.get(parameters, TRUtil.FIRMWARE_FILE_PATH);
            //context.Log("FirmwareUpgrade", "Firmware file: " + filePath);

            string firmwareVersion = Runner.Util.get(parameters, TRUtil.FIRMWARE_VERSION);
            //context.Log("FirmwareUpgrade", "Firmware version: " + firmwareVersion);

            context.SetParameter(TRUtil.FIRMWARE_VERSION, firmwareVersion);

            context.Report(this.GetType().Name, "Firmware version: " + firmwareVersion);


            //check file 
            if (!File.Exists(filePath))
            {
                context.Report(this.GetType().Name, filePath + "  file not found!!");
                return;
            }

            //read all bytes from file
            byte[] byteArray = File.ReadAllBytes(filePath);


            List<byte> fPacks = new List<byte>();
            fPacks.AddRange(Encoding.Default.GetBytes(firmwareVersion));

            //context.Log("FirmwareUpgrade", "Firmware Size in bytes: " + byteArray.Length);

            List<ByteChunk> chunks = ByteChunk.DoChunking(byteArray, TRUtil.CHUNK_SIZE, TRUtil.EOF_BYTE);

            int chunkCount = chunks.Count;
            //context.Log("FirmwareUpgrade", "Firmware Chunk count: " + chunkCount);

            byte[] chunkCountIn2Bytes = Packet.NumberToByteArray(chunkCount); // 2 bytes size

            byte[] packetContentLength = Packet.NumberToByteArray((1 + fPacks.Count + 2));
            List<byte> firstPacketBytes = new List<byte>();
            firstPacketBytes.AddRange(packetContentLength); // this will change if version string is of different lenght
            firstPacketBytes.Add(TRUtil.ZERO);
            firstPacketBytes.AddRange(fPacks);
            firstPacketBytes.AddRange(chunkCountIn2Bytes);


            Packet firstPacket = WritePacket.CreateFromBytes(TRUtil.SET_FIRMWARE_TYPE, TRUtil.FIRMWARE_SUBTYPE_FIRST, firstPacketBytes.ToArray());
            // context.Log("FirmwareUpgrade", "Firmware First Packet: " + firstPacket.ToRawByteString());


            packetList.Add(firstPacket);
            for (int i = 0; i < chunkCount; i++)
            {
                packetList.Add(createPacketFromByteChunk((i + 1), chunks[i]));
            }

            context.Log("FirmwareUpgrade", "Number of Firmware packets created: " + packetList.Count);

            context.Log("FirmwareUpgrade", "Initialization Complete!");

            isInitialised = true;
        }

        private Packet createPacketFromByteChunk(int index, ByteChunk chunk)
        {
            byte[] chunkIndex = Packet.NumberToByteArray(index); // - 0th index is 1st chunk 2 bytes size
            byte[] packetContentLength = Packet.NumberToByteArray((1 + chunk.Size + 2));

            List<byte> packetBytes = new List<byte>();
            packetBytes.AddRange(packetContentLength); // this will change if version string is of different lenght
            packetBytes.Add(TRUtil.ONE);
            packetBytes.AddRange(chunkIndex);
            packetBytes.AddRange(chunk.Chunk);


            return WritePacket.CreateFromBytes(TRUtil.SET_FIRMWARE_TYPE, TRUtil.FIRMWARE_SUBTYPE_CHUNK, packetBytes.ToArray());
        }

        protected override int CommandCount
        {
            get
            {
                return packetList.Count;
            }
        }

        protected override Command getNextCommand(int index, Command previous, ActionContext context, Dictionary<string, object> parameters)
        {
            //context.Log("FirmwareUpgrade", "Getting Next Command at Index: " + index);

            if (!isInitialised)
                Initialise(parameters, context);

            //if (index >= CommandCount)
            //    return null;

            //Analyse prev command response

            int chunkNumber = index; // default value - when previous response does not exist
            if (null != previous && previous.Response != null)
            {
                if (previous.Response.PrimaryType != "60")
                    throw new Exception("Invalid Reponse Type!!");

                if (previous.Response.SecondaryType == "81")
                    throw new ComPortResetException();

                Packet prevResponse = previous.Response;
                // context.Log("Firmware Upgrade", "Previous Command Response: " + prevResponse.ToRawByteString());

                int blobNo = prevResponse.GetByteAtIndex(11);
                int chunkNo = prevResponse.GetByteAtIndex(12);
                //context.Log("Firmware Upgrade", "Previous Response - Blob No, Chunk No: " + prevResponse.GetByteAtIndex(11) + "," + prevResponse.GetByteAtIndex(12));
                chunkNumber = blobNo * TRUtil.BYTE_SIZE + chunkNo;
            }

            //context.Log("Firmware Upgrade", "Requested Chunk Number: " + chunkNumber);

            if (packetList.Count <= chunkNumber)
                return null;


            return new Command("firmwareupgrade", packetList[chunkNumber], 10);
        }
    }
}