﻿using Runner;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;

namespace TRConfig
{
    public class ComPort : IDisposable
    {

        #region "Private Memebers"

        private ActionContext _context;
        private SerialPort _systemSerialPort;
        private List<Packet> _writtenPackets = new List<Packet>();
        private List<Packet> _readPackets = new List<Packet>();

        // hold the Comport properties for reopening
        private string _name;
        private int _baudRate;

        private ComPort(string name, int boudRate, ActionContext context)
        {
            this._context = context;
            _name = name;
            _baudRate = boudRate;
            DoOpen();
        }

        private ComPort DoOpen()
        {
            _context.Log(this.GetType().Name, _name + " opening...with baudrate :" + _baudRate);
            _systemSerialPort = new SerialPort(_name, _baudRate)
            {
                // Data must be written/read within a second
                ReadTimeout = 1000,
                WriteTimeout = 1000,
                // standard Endline windows style    
                NewLine = "\r\n",
            };

            // register event handler to process the data received.
            this._systemSerialPort.DataReceived += _systemSerialPort_DataReceived;

            this._systemSerialPort.Open();
            return this;
        }

        private SerialPort SystemSerialPort { get { return this._systemSerialPort; } }

        private void _systemSerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            System.Threading.Thread.Sleep(30);

            int Len = this.SystemSerialPort.BytesToRead;

            if (Len < 1)
            {
                return;
            }

            byte[] data = new byte[Len];
            this.SystemSerialPort.Read(data, 0, Len);
            //       this._context.Log(this.GetType().Name, "[COM_READ]:"+ BitConverter.ToString(data)); 
            if (data.Length > 0)
            {
                Packet getPacket = new ReadPacket(data);

                //         _context.Log(this.GetType().Name, "Read Packet:" + getPacket.ToRawByteString());

                if (LastRead != null && LastRead.Validate == Packet.PacketStatus.Incomplete)
                {
                    this.LastRead.Append(data);
                }
                else
                {
                    this.ReadPackets.Add(getPacket);
                }

                if (LastRead.Validate != Packet.PacketStatus.Incomplete)
                {
                    Command currentCommand = _context.GetParameter<Command>(TRUtil.COMMAND);
                    currentCommand.State = Command.CommandState.Received;
                }
            }
        }

        internal void Reset()
        {
            Close();
            System.Threading.Thread.Sleep(1000);
            DoOpen();
        }

        #endregion

        #region "Static Memebers"

        internal static ComPort Open(string name, int boudRate, ActionContext context)
        {
            return new ComPort(name, boudRate, context);
        }

        #endregion

        #region "Public Members"

        public string GetName { get { return this.SystemSerialPort.PortName; } }

        public int GetBoudRate { get { return this.SystemSerialPort.BaudRate; } }

        public bool IsOpen
        {
            get
            {
                return this.SystemSerialPort.IsOpen;
            }
        }

        public List<Packet> WrittenPackets { get { return this._writtenPackets; } }

        public List<Packet> ReadPackets { get { return this._readPackets; } }

        public Packet LastWritten { get { return this.WrittenPackets.Count > 0 ? this.WrittenPackets.Last() : null; } }

        public Packet LastRead { get { return this.ReadPackets.Count > 0 ? this.ReadPackets.Last() : null; } }

        public void Write(Packet packet, bool isSpaceEnter)
        {
            this._writtenPackets.Add(packet);
            Command currentCommand = _context.GetParameter<Command>(TRUtil.COMMAND);
            currentCommand.State = Command.CommandState.Sent;


            // _context.Log(this.GetType().Name, "Write Packet:" + packet.ToRawByteString());

            this.SystemSerialPort.Write(packet.RawBytes.ToArray(), 0, packet.RawBytes.Count);
            if (isSpaceEnter)
            {
                this.SystemSerialPort.Write("\r\n");
            }
        }

        public void Write(Packet packet)
        {
            Write(packet, false);

            //this._writtenPackets.Add(packet);
            //Command currentCommand = _context.getParameter<Command>(TRUtil.COMMAND);
            //currentCommand.State = Command.CommandState.Sent;


            //_context.Log(this.GetType().Name, "Write Packet:" + packet.ToRawByteString());

            //this.SystemSerialPort.Write(packet.RawBytes.ToArray(), 0, packet.RawBytes.Count);
            //this.SystemSerialPort.Write("\r\n");
        }

        public void Close()
        {
            this._systemSerialPort.DataReceived -= this._systemSerialPort_DataReceived;

            this._systemSerialPort.Close();


            _context.Log(this.GetType().Name, this.SystemSerialPort.PortName + " closed...");

        }

        public void Dispose()
        {
            this.Close();
            this._writtenPackets = null;
            this._readPackets = null;
            this._systemSerialPort = null;
        }

        #endregion
    }
}
