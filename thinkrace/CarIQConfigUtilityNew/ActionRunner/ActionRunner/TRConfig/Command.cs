﻿using Runner;
using System.Threading;

namespace TRConfig
{
    public class Command
    {
        public enum CommandState
        {
            Initialised = 0,
            Sent = 1,
            Received = 2,
            Error = 3,
            Complete = 4
        }

        public Command(string name, Packet request)
        {
            this.Name = name;
            State = CommandState.Initialised;
            Request = request;
            SleepTime = TRUtil.READ_WAIT_MILLISECOND;
        }

        public Command(string name, Packet request, int sleepTime)
        {
            this.Name = name;
            State = CommandState.Initialised;
            Request = request;
            SleepTime = sleepTime;
        }

        public Packet Request { get; set; }
        public Packet Response { get; set; }
        public int SleepTime { get; set; }

        public string Name { get; set; }

        public CommandState State { get; set; }

        // Execute command using the request
        public void execute(ActionContext context, ComPort port)
        {
            // write to com port
            if (State == CommandState.Initialised)
            {
                port.Write(Request);
            }
            else
            {
                // Hack - don't rewrite, but assume it is sent
                State = CommandState.Sent;
            }
            int counter = 0, MAX_ATTEMPTS = 50;
            while (State != CommandState.Received && counter < MAX_ATTEMPTS)
            {
                counter++;
                Thread.Sleep(SleepTime);
            }

            // If we have reached max attempts, it means - no response!
            if (counter < MAX_ATTEMPTS)
            {
                //context.Log(this.GetType().Name, "Last Read: " + port.LastRead.ToString());
                //context.Log(this.GetType().Name, "Last Packet Bytes: " + port.LastRead.ToRawByteString());
                Response = port.LastRead;
            }
            else
            {
                //context.Log(this.GetType().Name, "No Response!!");
            }
        }
    }
}
