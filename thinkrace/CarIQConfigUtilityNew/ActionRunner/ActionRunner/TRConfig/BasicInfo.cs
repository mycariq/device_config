﻿using Runner;
using System.Collections.Generic;

namespace TRConfig
{
    class BasicInfo : TRConfigActor
    {
        static string[] requests = { "2800000000000000030001000229", "2800000000000000020001000329", "2800000000000010060001001729" };
        static string[] commandNames = { "Query DeviceId", "Query Version", "Query IMEI" };

        protected override int CommandCount
        {
            get
            {
                return requests.Length;
            }
        }

        protected override Command getNextCommand(int index, Command previous, ActionContext context, Dictionary<string, object> parameters)
        {

            //save results in context for csv file
            if (previous != null && previous.Response != null)
            {
                switch (index)
                {
                    case 1:
                        context.SetParameter(TRUtil.DEVICE_ID, previous.Response.DeviceID);
                        break;

                    case 2:
                        context.SetParameter(TRUtil.FIRMWARE_VERSION, previous.Response.Text);
                        break;

                    case 3:
                        context.SetParameter(TRUtil.IMEI, previous.Response.Text);
                        break;
                    default:
                        break;
                }
            }

            if (index >= requests.Length)
                return null;

            return new Command(commandNames[index], WritePacket.CreateFromText(requests[index]));
        }
    }
}
