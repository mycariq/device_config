﻿using Runner;
using System.Collections.Generic;

namespace TRConfig
{
    class SetServer : TRConfigActor
    {
        protected override Command getNextCommand(int index, Command previous, ActionContext context, Dictionary<string, object> parameters)
        {
            //save results in context for csv file
            if (previous != null && previous.Response != null)
            {
                string[] results = previous.Response.Text.Split(',');

                if (results.Length >= 2)
                {
                    context.SetParameter(TRUtil.DEVICE_ID, previous.Response.DeviceID);
                    context.SetParameter(TRUtil.SERVER, results[0]);
                    context.SetParameter(TRUtil.PORT, results[1]);
                    context.SetParameter(TRUtil.APN, results[2]);

                }

            }

            if (index >= CommandCount)
                return null;

            string srvAddress = Runner.Util.get(parameters, TRUtil.SERVER);
            string srvPort = Runner.Util.get(parameters, TRUtil.PORT);
            string apn = Runner.Util.get(parameters, TRUtil.APN);

            byte servertype = (index == 1) ? TRUtil.MAIN_SERVER_SUBTYPE : TRUtil.STANDBY_SERVER_SUBTYPE;

            Packet request = WritePacket.CreateFromText(TRUtil.SET_SERVER_TYPE, servertype, srvAddress + "," + srvPort + "," + apn);

            return new Command("SetServer", request);
        }

        protected override int CommandCount
        {
            get
            {
                return 2;
            }
        }
    }
}
