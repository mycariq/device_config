﻿using System;
using System.Collections.Generic;

namespace TRConfig
{
    class ByteChunk
    {
        public ByteChunk(byte[] chunk)
        {
            Chunk = chunk;
        }

        public byte[] Chunk { get; set; }

        public int Size { get { return Chunk.Length; } }

        public static List<ByteChunk> DoChunking(byte[] array, int chunkSize, byte padding)
        {
            List<ByteChunk> returnVal = new List<ByteChunk>();

            int arraySize = array.Length;
            int chunkCount = arraySize / chunkSize;
            if ((arraySize % chunkSize) != 0)
                chunkCount++;

            for (int i = 0; i < chunkCount; i++)
            {
                int startIndex = chunkSize * i;

                returnVal.Add(getChunk(array, startIndex, chunkSize, padding));


            }
            return returnVal;
        }

        private static ByteChunk getChunk(byte[] array, int startIndex, int chunkSize, byte padding)
        {
            // create array to return
            byte[] byteArr = new byte[chunkSize];

            // original array is of size less than chunksize
            int arrayLength = array.Length; // 513
            int copyLength = arrayLength - startIndex; // 513-512 = 1

            if (copyLength >= chunkSize)
            {
                Array.Copy(array, startIndex, byteArr, 0, chunkSize); // copy 1 bytes
            }
            else
            {
                Array.Copy(array, startIndex, byteArr, 0, copyLength); // copy 1 bytes

                for (int i = copyLength; i < chunkSize; i++)
                {
                    byteArr[i] = padding;
                }
            }

            return new ByteChunk(byteArr);
        }
    }
}
