﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TRConfig
{
    public class ReadPacket : Packet
    {
        public ReadPacket(byte[] rawBytes) : base(rawBytes)
        {

        }

        public override void Append(byte[] bytes)
        {
            this.RawBytes.AddRange(bytes);
        }
    }
}
