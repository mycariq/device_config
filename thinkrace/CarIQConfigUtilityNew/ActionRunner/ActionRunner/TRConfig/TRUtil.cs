﻿using System;
using System.Linq;

namespace TRConfig
{
    class TRUtil
    {
        public static string COMPORT = "ComPort";
        public static byte SET_SERVER_TYPE = 0x10;
        public static byte MAIN_SERVER_SUBTYPE = 0x01;
        public static byte STANDBY_SERVER_SUBTYPE = 0x02;
        public static int DEFAULT_BAUD_RATE = 115200;
        //public static int FIRMWARE_DEPLOY_BAUD_RATE = 10416;
        public static int CHUNK_SIZE = 512;

        public static string SERVER = "server";
        public static string PORT = "port";
        public static string APN = "apn";
        internal static string COMMAND = "COMMAND";

        public static string FIRMWARE_FILE_PATH = "filepath";
        public static string FIRMWARE_VERSION = "version";
        public static byte SET_FIRMWARE_TYPE = 0x60;
        public static byte FIRMWARE_SUBTYPE_FIRST = 0x01;
        public static byte EOF_BYTE = 0xFF;
        public static byte ZERO = 0x00;
        public static byte ONE = 0x01;
        public static byte FIRMWARE_SUBTYPE_CHUNK = 0x02;
        public static readonly int BYTE_SIZE = 256;
        internal static int READ_WAIT_MILLISECOND = 10;//300;

        public static string IMEI = "imei";

        public static string DEVICE_ID = "deviceid";
        public static string CSVFILENAME = "csvfilename";


        internal static byte[] getBytes(byte[] byteArray, int size)
        {
            if (byteArray.Length == size)
                return byteArray;

            if (byteArray.Length > size)
                return byteArray.Skip(0).Take(size).ToArray();

            throw new NotImplementedException();
        }

        internal static byte[] getNullBytes(int size)
        {
            byte[] retval = new byte[size];
            for (int i = 0; i < size; i++)
            {
                retval[i] = 0x00;
            }

            return retval;
        }
    }
}