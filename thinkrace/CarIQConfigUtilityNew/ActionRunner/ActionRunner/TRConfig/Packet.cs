﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TRConfig
{
    public abstract class Packet
    {

        #region "Private Members"

        private List<byte> _rawbytes;

        //stream of bytes with set protocol
        //START-DATA-CHECKSUM-END
        public Packet(byte[] byteArray)
        {
            this._rawbytes = new List<byte>();
            this._rawbytes.AddRange(byteArray);
        }

        internal byte CalculateChecksum
        {
            get
            {
                return CalCheckSum(GetSubPacket(1, this.ResolvedBytes.Length - 3));
                //return CalCheckSum(GetSubPacket(1, this.RawBytes.Count - 3));
            }
        }

        internal static byte CalCheckSum(byte[] _PacketData)
        {
            Byte _CheckSumByte = 0x00;
            for (int i = 0; i < _PacketData.Length; i++)
                _CheckSumByte ^= _PacketData[i];
            return _CheckSumByte;
        }

        internal string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        #endregion

        #region "Public Members"

        public enum PacketStatus
        {
            InValid = 0,
            Incomplete = 1,
            CompleteInvalid = 2,
            Valid = 3
        }

        // This should find from the rawbytes whether
        // a. This is a complete and valid packet - this is true when first and last byte are correct, checksum is correct and length byte is correct, plus additional checks if required
        // b. Or, is this incomplete but valid packet - this is true when first byte is correct, but last byte is not correct. Such packet can be in progress
        // c. or, is this Invalid packet - in this case first byte is wrong. (plus additional checks in future)
        // Logic should be - check if this packet is invalid - if so, return
        //  Then check if it is incomplete (by looking at last byte) - if so return
        // Lastly do a deep check of first byte, last byte and checksum - and return valid if checks succeed
        public virtual PacketStatus Validate
        {
            get
            {
                if (this.RawBytes.Count <= 0)
                    return PacketStatus.InValid;


                if (!(this.RawBytes.First() == START))
                    return PacketStatus.InValid;

                if (!(this.RawBytes.Last() == END))
                    return PacketStatus.Incomplete;

                // it is complete - check for checksum
                return this.CheckSum == CalculateChecksum ? PacketStatus.Valid : PacketStatus.CompleteInvalid;
            }
        }

        public string DeviceID { get { return this.ByteArrayToString(GetSubPacket(1, 6)); } }

        public string PrimaryType { get { return this.ByteArrayToString(GetSubPacket(7, 1)); } }

        public string SecondaryType { get { return this.ByteArrayToString(GetSubPacket(8, 1)); } }

        public string Text { get { return Encoding.GetEncoding("GB2312").GetString(GetSubPacket(11, this.ResolvedBytes.Length - 2 - 11)); } }

        public int TextLength { get { return Text.Length; } }

        //public void Append(byte[] bytes)
        //{
        //    this._rawbytes.AddRange(bytes);
        //}

        public override string ToString()
        {
            return "DeviceID:" + DeviceID + ",PrimaryType:" + PrimaryType + ",SecondaryType:" + SecondaryType
                + ",Text:" + Text + ",CheckSum:" + CheckSum + ",Calculated Checksum:" + CalculateChecksum;
            //+                ",RawMessage:" + Message;
        }

        public string ToRawString()
        {
            return Encoding.GetEncoding("GB2312").GetString(RawBytes.ToArray());
        }

        public string ToRawByteString()
        {
            return BitConverter.ToString(RawBytes.ToArray());
        }

        public int Length { get { return this._rawbytes.Count; } }

        public List<byte> RawBytes
        {
            get { return this._rawbytes; }
            set { this._rawbytes = value; }
        }

        public string Message
        {
            get
            {
                return (this.RawBytes.Count > 0 ? Encoding.GetEncoding("GB2312").GetString(this.RawBytes.ToArray()) : string.Empty);
            }
        }

        public byte GetByteAtIndex(int index)
        {
            //return this._rawbytes[index];
            return ResolvedBytes[index];
        }

        //public byte[] GetSubPacket(int startIndex)
        //{
        //    return this.RawBytes.Skip(startIndex).Take(this.RawBytes.Count - startIndex).ToArray();
        //}

        public byte[] ResolvedBytes
        {
            get
            {
                List<byte> resolvedBytes = new List<byte>();
                //Add START
                //Adding Escape Sequence
                bool escapedState = false;
                for (int index = 0; index < RawBytes.Count; index++)
                {
                    // If in escaped state, apply xoring
                    if (escapedState)
                    {
                        byte toEscape = RawBytes[index];
                        toEscape ^= ESCAPE;
                        resolvedBytes.Add(toEscape);
                        escapedState = false;
                        continue;
                    }

                    // If ESCAPE, just set the state
                    if (RawBytes[index] == ESCAPE)
                    {
                        escapedState = true;
                        continue;
                    }

                    resolvedBytes.Add(RawBytes[index]);
                }

                return resolvedBytes.ToArray();
            }
        }

        public byte[] GetSubPacket(int startIndex, int length)
        {
            return ResolvedBytes.Skip(startIndex).Take(length).ToArray();
        }

        //public byte CheckSum
        //{
        //    get
        //    {
        //        if (RawBytes.Count < 4)
        //            return 0x00;

        //        byte retbyte = GetByteAtIndex(this.RawBytes.Count - 2);
        //        if (GetByteAtIndex(this.RawBytes.Count - 3) == ESCAPE)
        //        {
        //            retbyte ^= ESCAPE;
        //        }
        //        return retbyte;
        //    }
        //}

        public byte CheckSum
        {
            get
            {
                if (ResolvedBytes.Length < 4)
                    return 0x00;

                return GetByteAtIndex(this.ResolvedBytes.Length - 2);
                //if (GetByteAtIndex(this.RawBytes.Count - 3) == ESCAPE)
                //{
                //    retbyte ^= ESCAPE;
                //}
                //return retbyte;
            }
        }

        #endregion

        #region "Static Memebers"

        internal static byte ESCAPE { get { return 0x3D; } }

        internal static byte START { get { return 0x28; } }

        internal static byte END { get { return 0x29; } }

        //internal static Packet CreateFromMessage(string message)
        //{
        //    return new Packet(StringToByteArray(message), true);
        //}

        //internal static Packet CreateFromMessage(string message, bool isRaw)
        //{
        //    return new Packet(StringToByteArray(message), isRaw);
        //}

        //internal static Packet CreateFromRawBytes(byte[] bytes)
        //{
        //    return new Packet(bytes, true);
        //}

        //internal static Packet CreateFromBytesMessage(byte[] bytes)
        //{
        //    return new Packet(bytes, true);
        //}

        //internal static Packet CreateFromBytesMessage(byte[] bytes, bool isRaw)
        //{
        //    return new Packet(bytes, isRaw);
        //}

        //internal static Packet CreateFromFileOffset(FileInfo file, int offset, int size)
        //{
        //    return new Packet(null, false);
        //}

        internal static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        internal static byte[] NumberToByteArray(int num)
        {

            byte[] bytes = new byte[2];
            bytes[0] = (byte)(num / TRUtil.BYTE_SIZE);
            bytes[1] = (byte)(num % TRUtil.BYTE_SIZE);

            return bytes;
        }

        public virtual void Append(byte[] bytes)
        {
            throw new Exception("Can not append to a generic packet!");
        }

        #endregion

    }
}
