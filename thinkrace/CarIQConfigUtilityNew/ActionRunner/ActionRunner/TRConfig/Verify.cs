﻿using System.Collections.Generic;
using Runner;

namespace TRConfig
{
    class Verify : Actor
    {
        protected override void run(ActionContext context, Dictionary<string, object> parameters)
        {
            context.Log(this.GetType().Name, "I am " + this.GetType().FullName);

            string srvAddress = Runner.Util.get(parameters, TRUtil.SERVER);
            string srvPort = Runner.Util.get(parameters, TRUtil.PORT);
            string apn = Runner.Util.get(parameters, TRUtil.APN);
            string firmwareVersion = Runner.Util.get(parameters, TRUtil.FIRMWARE_VERSION);
            string csvfilename = Runner.Util.get(parameters, TRUtil.CSVFILENAME);

            if (csvfilename == null)
                csvfilename = "VerifyDevice";

            string deviceID = context.GetParameter<string>(TRUtil.DEVICE_ID);

            string setServer = context.GetParameter<string>(TRUtil.SERVER);
            VerifyLog(context, "Server", setServer, srvAddress);

            string setPort = context.GetParameter<string>(TRUtil.PORT);
            VerifyLog(context, "Port", setPort, srvPort);

            string setAPN = context.GetParameter<string>(TRUtil.APN);
            VerifyLog(context, "APN", setAPN, apn);

            string SetVersion = context.GetParameter<string>(TRUtil.FIRMWARE_VERSION);
            VerifyLog(context, "Version", SetVersion, firmwareVersion);



            context.CSVReport(csvfilename, deviceID + "," + SetVersion + "," + setServer + "," + setPort + "," + setAPN);
        }

        private void VerifyLog(ActionContext context, string key, string source, string destination)
        {

            string strMsg = "Set " + key + ":" + source + ", Verify " + key + ":" + destination + System.Environment.NewLine + key + " verification " + ((source == destination) ? "Successfully!!" : "falied!!");
            context.Log("Verify", strMsg);
            context.Report("Verify", strMsg);
        }
    }
}
