﻿using System;
using Runner;

namespace DeviceConfig
{
    class Program
    {
        static void Main(string[] args)
        {
            ActionContext context = new ActionContext();

            try
            {
                ActionParameters parametrs = new ActionParameters(args, context);

                if (!parametrs.IsValid)
                {
                    //parametrs.FilePath = AppDomain.CurrentDomain.BaseDirectory + "testcariq.json";
                    //context.SetParameter(TRConfig.TRUtil.COMPORT, "COM3");
                    ////Console.WriteLine("Press enter key to exit..");
                    //Console.ReadLine();
                    return;
                }
                else
                {
                    context.Log("Program", "FilePath:" + parametrs.FilePath);
                    context.Log("Program", "Port:" + parametrs.PortName);
                    context.SetParameter(TRConfig.TRUtil.COMPORT, parametrs.PortName);
                }

                //context.Log("Program", "Action Runner initialising....");
                ActionRunner runn = new ActionRunner(parametrs.FilePath, context);
                //  context.Log("Program", "Action Runner executing....");

                runn.Execute();

                //Console.WriteLine("Press enter key to exit..");
                //Console.ReadLine();
            }
            catch (Exception ex)
            {
                context.Log("Program Exception!!", ex.Message);

                context.Log("Program", ex.StackTrace);
                context.Log("Inner Exception", ex.InnerException != null ? ex.InnerException.ToString() : "none");
            }

        }


    }
}
