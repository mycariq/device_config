{
  "actions": [
    {
      "name": "step1",
      "params": {
        "dispaly": "Hi1",
        "action": "test1"
      },
      "class": "Test.TestActor1"
    },
    {
      "name": "step2",
      "params": {
        "dispaly": "Hi2",
        "action" :"test2"
      },
      "class": "Test.TestActor2"
    },
    {
      "name": "step3",
      "params": {
        "dispaly": "Hi3",
        "action": "test3"
      },
      "class": "Test.TestActor3"
    },
	{
      "name": "step4",
      "params": {
        "dispaly": "BurnFirmware",
        "action": "BurnFirmware"
      },
      "class": "TRConfig.BurnFirmware"
    }
  ]
}