﻿using System.IO;
using System.IO.Ports;

namespace Runner
{
    public class ActionParameters
    {

        public string FilePath { get; set; }

        public string PortName { get; set; }

        private bool _isvalid = true;
        public bool IsValid { get { return _isvalid; } }

        public ActionParameters(string[] args, ActionContext context)
        {
            #region "Getting Port List"
            //getting port list
            string[] ports = SerialPort.GetPortNames();

            if (ports.Length > 0)
            {
                context.Log(this.GetType().Name, "Avaliable comports:");
                context.Report(this.GetType().Name, "Avaliable comports:");

                foreach (string s in SerialPort.GetPortNames())
                {
                    context.Log(this.GetType().Name, s);
                    context.Report(this.GetType().Name, s);
                }
            }
            else
            {
                context.Log(this.GetType().Name, "No com port founds");
                context.Report(this.GetType().Name, "No com port founds!!");

            }
            #endregion

            #region "Validate Config.json file"

            if (args.Length == 0)
            {
                context.Log(this.GetType().Name, "Please enter config file path");
                context.Report(this.GetType().Name, "Please enter config file path");
                _isvalid = false;
                return;
            }

            this.FilePath = args[0];

            if (!File.Exists(this.FilePath))
            {
                context.Log(this.GetType().Name, "file not found");
                context.Report(this.GetType().Name, "file not found!!");
                _isvalid = false;
                return;
            }

            //if (!(Path.GetExtension(this.FilePath).ToLower() == "json"))
            //{
            //    context.Log(this.GetType().Name, "not a valid json file");
            //    context.Report(this.GetType().Name, "not a valid json file!!");
            //    _isvalid = false;
            //    return;
            //}

            #endregion

            if (args.Length == 2)
            {
                bool _isAvailable = false;
                foreach (string name in ports)
                {
                    if (name.ToLower().Trim() == args[1].ToString().ToLower().Trim())
                    {
                        _isAvailable = true;
                        break;
                    }
                }

                if (!_isAvailable)
                {
                    context.Log(this.GetType().Name, "No com port founds in avalible port list.");
                    context.Report(this.GetType().Name, "No com port founds in avalible port list");
                    _isvalid = false;
                    return;
                }
                this.PortName = args[1];
            }

            switch (ports.Length)
            {
                case 0:
                    this.PortName = "0";
                    _isvalid = false;
                    break;

                case 1:
                    this.PortName = ports[0];
                    break;

                //case 2:
                //    Console.WriteLine("");
                //    break;

                default:

                    break;
            }


        }
    }
}