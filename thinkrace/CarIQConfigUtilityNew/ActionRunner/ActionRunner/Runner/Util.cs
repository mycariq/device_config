﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runner
{
    class Util
    {
        public static string CONFIG_ACTION = "actions";
        public static string CONFIG_NAME = "name";
        public static string CONFIG_PARAMS = "params";
        public static string CONFIG_CLASS = "class";

        public static string get(Dictionary<string, object> parameters, String key)
        {
            return parameters[key].ToString().Replace(" ", "").Replace(",", "");
        }
    }
}
