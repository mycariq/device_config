﻿using System.Collections.Generic;

namespace Runner
{
    public abstract class Actor
    {

        public void start(ActionContext context, Dictionary<string, object> parameters)
        {
            // context.Log(this.GetType().Name, "Actor Start...");
            run(context, parameters);
        }

        protected abstract void run(ActionContext context, Dictionary<string, object> parameters);

    }
}