﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Runner
{
    public class ActionContext
    {
        // string filePath;
        public ActionContext()
        {
            this.Parameters = new Dictionary<string, object>();
            //filePath = AppDomain.CurrentDomain.BaseDirectory + "CQConfig.log";
            //if (File.Exists(filePath))
            //    return;

            //using (FileStream fs = File.Create(filePath))
            //{

            //}
        }

        private Dictionary<string, object> Parameters { get; set; }

        public T GetParameter<T>(String key)
        {
            return (T)Parameters[key];
        }

        public void SetParameter(String key, object value)
        {
            Parameters[key] = value;
        }

        public void Report(string moduleName, string message)
        {
            string logMessage = "====================================\n " + message;
            Console.WriteLine(logMessage);


            //    //write in log file
            //    CreateLogFile(logMessage + Environment.NewLine, AppDomain.CurrentDomain.BaseDirectory + "runnerlogs.log");
            //
        }

        public void Log(string moduleName, string message)
        {
            string logMessage = DateTime.Now.ToString() + ",TH-" + Thread.CurrentThread.ManagedThreadId.ToString() + "," + moduleName + "," + message;
            //Console.WriteLine(logMessage);

            //write in log file
            //appendToFile(logMessage + Environment.NewLine, AppDomain.CurrentDomain.BaseDirectory + "runnerlogs.log");
            AppendToFile(logMessage + Environment.NewLine);
        }

        //private void AppendToFile(String message)
        //{
        //    try
        //    {
        //        File.AppendAllText(filePath, message);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        private void AppendToFile(String message)
        {
            try
            {


                string fileName = Parameters.ContainsKey(TRConfig.TRUtil.COMPORT) ? GetParameter<string>(TRConfig.TRUtil.COMPORT) + "Config.log" : "CQConfig.log";

                //fileName = (fileName == null) ? "CQConfig.log" : fileName + "Config.Log";

                string logFilePath = AppDomain.CurrentDomain.BaseDirectory + fileName;

                if (File.Exists(logFilePath))
                {
                    File.AppendAllText(logFilePath, message);
                }
                else
                {
                    using (FileStream fs = File.Create(logFilePath)) { }
                    File.AppendAllText(logFilePath, message);
                }
            }
            catch (Exception ex)
            {

            }
        }

        internal void CSVReport(string name, string details)
        {
            string csvFilePath = AppDomain.CurrentDomain.BaseDirectory + name + ".csv";
            string _message = DateTime.Now.ToString() + "," + details + Environment.NewLine;
            if (File.Exists(csvFilePath))
            {
                File.AppendAllText(csvFilePath, _message);
            }
            else
            {
                using (FileStream fs = File.Create(csvFilePath)) { }

                File.AppendAllText(csvFilePath, _message);
            }
        }
    }
}