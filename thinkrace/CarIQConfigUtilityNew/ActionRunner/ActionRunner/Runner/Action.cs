﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Runner
{
    public class Action
    {
        private string _actionName;
        private Dictionary<string, object> _params;
        private string _classname;

        public Action(string name, Dictionary<string, object> context, string className)
        {
            this._actionName = name;
            this._params = context;
            this._classname = className;
        }

        public void Execute(ActionContext context)
        {
            //   context.Log(this.GetType().Name, "Actor Initializing...");
            Actor a = (Actor)Activator.CreateInstance(Type.GetType(this._classname));
            a.start(context, this._params);

        }

        public string ActionName { get { return _actionName; } }

        internal static Action Create(JToken actionJson)
        {
            return new Action(actionJson[Util.CONFIG_NAME].ToString(), actionJson[Util.CONFIG_PARAMS].ToObject<Dictionary<string, object>>(), actionJson[Util.CONFIG_CLASS].ToString());
        }
    }
}