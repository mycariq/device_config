﻿using System.Collections.Generic;
using System.IO;

namespace Runner
{
    class ActionRunner
    {
        private List<Action> actionList = new List<Action>();
        private ActionContext _context = null;

        public ActionRunner(string configFilePath, ActionContext context)
        {
            this._context = context;

            //read config file 
            string json = File.ReadAllText(configFilePath);

            //get action list
            var actionListjson = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
            if (actionListjson != null)
            {
                foreach (var actionJson in ((Newtonsoft.Json.Linq.JObject)actionListjson)[Util.CONFIG_ACTION])
                {
                    actionList.Add(Action.Create(actionJson));

                }
            }

        }

        public void Execute()
        {
            _context.Report("ActionRunner", "=========================\nStart...");

            int actionCount = actionList.Count, indx = 1;
            foreach (Action act in actionList)
            {
                _context.Report("ActionRunner", "Executing Action " + (indx++) + " out of " + actionCount + " : " + act.ActionName);

                act.Execute(this._context);
            }
        }
    }
}