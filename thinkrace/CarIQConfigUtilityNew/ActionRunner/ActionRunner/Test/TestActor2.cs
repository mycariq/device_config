﻿using Runner;
using System.Collections.Generic;

namespace Test
{
    class TestActor2 : Actor
    {
        protected override void run(ActionContext context, Dictionary<string, object> parameters)
        {
            context.Log(this.GetType().Name, "I am testactor2");

            //foreach (var item in context.Parameters)
            //{
            //    context.Log(this.GetType().Name, "context:" + item);
            //}

            //foreach (var item in parameters)
            //{
            //    context.Log(this.GetType().Name, "Parameters:" + item);
            //}
        }
    }
}
